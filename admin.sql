-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 06, 2019 at 05:31 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `admin`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` int(11) NOT NULL,
  `admin_first_name` varchar(90) NOT NULL,
  `admin_last_name` varchar(25) NOT NULL,
  `email` varchar(54) NOT NULL,
  `phone` varchar(11) NOT NULL,
  `profile` varchar(233) NOT NULL,
  `national_id` int(11) NOT NULL,
  `password` varchar(33) NOT NULL,
  `hash` varchar(103) NOT NULL,
  `comp_id` int(12) NOT NULL,
  `active` int(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `admin_first_name`, `admin_last_name`, `email`, `phone`, `profile`, `national_id`, `password`, `hash`, `comp_id`, `active`) VALUES
(1, 'Emmanuel Kamanzi', 'Kamanzi', 'ntwariegide2@gmail.com', '0780577314', '5.jpg', 1123456, '12345', '320722549d1751cf3f247855f937b982', 1, 1),
(2, '', '', 'kwizemmy2004@gmail.com', '', '', 0, '', '5dd9db5e033da9c6fb5ba83c7a7ebea9', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE `application` (
  `id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `email` varchar(54) NOT NULL,
  `cv` varchar(266) NOT NULL,
  `status` int(11) NOT NULL,
  `task_id` int(3) NOT NULL,
  `manager` int(3) NOT NULL,
  `applicant` int(3) NOT NULL,
  `interview` varchar(255) NOT NULL DEFAULT '',
  `national_id` int(16) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `country` varchar(23) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`id`, `full_name`, `email`, `cv`, `status`, `task_id`, `manager`, `applicant`, `interview`, `national_id`, `phone`, `country`) VALUES
(25, 'Ntwari simeo', 'ntwariegide2@gmail.com', 'nodejs term iii - upto expressjs.pdf', 2, 4, 7, 1, ' thisis amdin decteting how the app is being done', 2147483647, '07884375734', 'Rwanda'),
(28, '$full_name', '$email', '$name', 0, 0, 0, 0, '', 0, '$phone', '$country'),
(29, 'Ntwari egide', 'ntwariegide2@gmail.com', 'install_guide.pdf', 2, 4, 7, 1, '', 324565767, '213445678', 'Rwanda'),
(30, 'Kalisa John', 'kalisa@gmail.com', 'cvs_Maths 2nd term.pdf', 2, 6, 1, 1, '', 1109837464, '078980345', 'Rwanda');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `comment_id` int(10) NOT NULL,
  `post_id_c` int(10) NOT NULL,
  `user_id_c` int(10) NOT NULL,
  `comment` text NOT NULL,
  `comment_time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`comment_id`, `post_id_c`, `user_id_c`, `comment`, `comment_time`) VALUES
(0, 4, 1, 'hello this is america', '2019-11-06 11:59:40'),
(0, 4, 1, 'Hello isac munyakazi', '2019-11-06 12:01:55'),
(0, 3, 1, 'Intresting', '2019-11-06 12:12:19');

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `comp_id` int(3) NOT NULL,
  `comp_name` varchar(25) NOT NULL,
  `comp_task` varchar(255) NOT NULL,
  `num_of_workers` int(14) NOT NULL,
  `CEO` varchar(23) NOT NULL,
  `Website` varchar(233) NOT NULL,
  `comp_profile` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`comp_id`, `comp_name`, `comp_task`, `num_of_workers`, `CEO`, `Website`, `comp_profile`) VALUES
(1, 'irembo', 'ICT innovation in Rwanda', 200, '', 'www.irembo.rw', '');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `noti_id` int(10) NOT NULL,
  `pos_id` int(10) NOT NULL,
  `post_usr` int(10) NOT NULL,
  `comm_user` int(10) NOT NULL,
  `comment` text NOT NULL,
  `status` tinyint(1) NOT NULL,
  `from_appl` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `project` varchar(100) NOT NULL,
  `experience` varchar(100) NOT NULL,
  `skill` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`noti_id`, `pos_id`, `post_usr`, `comm_user`, `comment`, `status`, `from_appl`, `time`, `project`, `experience`, `skill`) VALUES
(13, 0, 0, 1, '', 0, 0, '2019-11-06 06:07:38', '', '', ''),
(14, 0, 0, 1, 'Yes hello thsis is admin', 0, 0, '2019-11-06 06:07:57', '', '', ''),
(15, 4, 0, 1, 'hello this is america', 0, 0, '2019-11-06 11:59:40', '', '', ''),
(16, 4, 0, 1, 'Hello isac munyakazi', 0, 0, '2019-11-06 12:01:55', '', '', ''),
(17, 3, 0, 1, 'Intresting', 0, 0, '2019-11-06 12:12:19', '', '', ''),
(20, 4, 1, 1, 'Please mr dont be let for the interview which will be at august', 0, 1, '2019-11-06 13:06:18', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `post_id` int(3) NOT NULL,
  `comp_id` int(3) NOT NULL,
  `career_name` varchar(255) NOT NULL,
  `career_desc` varchar(255) NOT NULL,
  `posted_date` date NOT NULL,
  `comp_profile` varchar(255) NOT NULL,
  `like` int(11) NOT NULL,
  `unlike` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`post_id`, `comp_id`, `career_name`, `career_desc`, `posted_date`, `comp_profile`, `like`, `unlike`) VALUES
(6, 1, 'Software Engineer', 'The one who must apply must have masters in software engineering so try to apply before the end of this year.\r\nThank you !', '2019-11-06', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `p_id` int(11) NOT NULL,
  `Project_name` varchar(100) NOT NULL,
  `Proejct_lang` varchar(100) NOT NULL,
  `user` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project`
--

INSERT INTO `project` (`p_id`, `Project_name`, `Proejct_lang`, `user`) VALUES
(1, 'Social network', 'PHP', 'gurtej');

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE `registration` (
  `usr_id` int(10) NOT NULL,
  `name` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `image` varchar(250) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `password` varchar(250) NOT NULL,
  `project` varchar(100) NOT NULL,
  `experience` varchar(100) NOT NULL,
  `skill` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration`
--

INSERT INTO `registration` (`usr_id`, `name`, `email`, `image`, `gender`, `password`, `project`, `experience`, `skill`) VALUES
(1, 'ntwari', 'ntwari@gmail.com', 'blog-widget-02.png', 'MALE', '12345', 'Core Php Project this is new', 'Hello this is edit checker', 'I am experineced Wlordpress develpoer in rca'),
(2, 'sano', 'sano@gmail.com', '1888448_898409750258226_6887486239911226423_n.jpg', '', '123', 'Social Netwrok, E commerce, Blogs- News portal', '4', 'I am a PHP- Mysqli - wordpress- drupal developer'),
(3, 'Mugisha', 'mugisha@gmail.com', 'rsz_new_doc_2017-02-26_2.jpg', '', '123', 'Ecommerce -Shonow.com,Shopingg.com\r\nSocialnetwork- Meetme.com', '4', 'Java PHP Drupal Majento Wordpress'),
(4, 'Fiston', 'fiston@gmail.com', 'blog-widget-03.png', '', '123', 'Social Network', '1', 'PHP JAVA'),
(7, 'kwizera', 'kwizera@gmail.com', '3.jpg', '', 'kwizera123', '', '', ''),
(8, 'shallon', 'shallon@gmail.com', 'my.png', '', 'shallon@gmail.com', '', '', ''),
(0, 'Didier', 'didier@gmail.com', '005-peter-dorcas.jpg', '', '123123', '', '', ''),
(0, 'Didier', 'didier@gmail.com', '005-peter-dorcas.jpg', '', '123123', '', '', ''),
(0, 'group', 'ntwariegide2@gmail.com', '', 'femal', '1232323423412432', '', '', ''),
(0, 'ntwari egi', 'ntwariegide3@gmail.com', 'photoedit.jpg', 'femal', '2134134134214313414', '', '', ''),
(0, 'ntwari egi', 'ntwariegide5@gmail.com', 'photoedit.jpg', 'femal', 'ertyreertyrewrttrreteww', '', '', ''),
(0, 'ntwari egi', 'ntwariegide@gmail.com', 'photoedit.jpg', 'male', '1234567', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_fName` varchar(200) NOT NULL,
  `user_lName` varchar(200) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `user_gender` varchar(200) NOT NULL,
  `user_password` varchar(200) NOT NULL,
  `user_profile` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `user_fName`, `user_lName`, `user_name`, `user_email`, `user_gender`, `user_password`, `user_profile`) VALUES
(1, 'irakiza', 'sinclliar', 'sinclliar', 'irakiza@gmail.com', 'male', '01234567', 'default_user_logo.png'),
(2, 'mark', 'alain', 'mikol', 'mark@gmail.com', 'male', 'mark123', 'default_user_logo.png'),
(3, 'shallon', 'kobusinge', 'sharon', 'shallon@gmail.com', 'femal', 'shallon@gmail.com', 'default_user2.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `application`
--
ALTER TABLE `application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company`
--
ALTER TABLE `company`
  ADD PRIMARY KEY (`comp_id`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`noti_id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `application`
--
ALTER TABLE `application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `company`
--
ALTER TABLE `company`
  MODIFY `comp_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `noti_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `post_id` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
