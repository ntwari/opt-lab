<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles/about.css">
    <title>OPTLab | About</title>
    <link href="https://fonts.googleapis.com/css?family=Playball|Tangerine|Teko&display=swap" rel="stylesheet">
</head>
<body>
    <nav class="nav" id="page-top">
        <div class="back-home">
            <a href="index.php"><i class="fas fa-less-than"></i> Back Home</a>
        </div>
        <div class="nav-buttons">
            <a href="User/signin.php">Log In</a>
            <a href="User/signup.php">Sign Up</a>
        </div>
    </nav>
    <div class="description">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="siple-description">
                    <h2>Opportunity Lab</h2>
                    <p>We are <b>OPTLab</b>, we are connectors to both managers and new workers,
                        our goal is to get all of jobs seekers connected to where they think ,
                        their opportunity is. The best thing to acheiv this is working with you,
                        We thank you verry much for beiing with us !
                    </p>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="about-company">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="more-about">
                        <h3 class="text-center">More About OPTLab</h3>
                        <p>tarted in 2019,in Nyabihu at Rwanda Coding Academy, for helping and connecting youth unemployed to 
                            companies which need workers and then.that's this online application was made <b>(Opportunity Lab).</b>
                            &emsp;Mainly formed for Rwanda,Africa and the rest of the World.
                            This product includes the serivces like online application for job,online 
                            intervie taking for applicant, this with others will be included in our product 
                            what you re needed on is to get intouched through signing up so that you can enjoy on what 
                            opportunity lab is bringing to you.
                            <span>Wether a User or an Admin of Company we wish you good time while using oour app !</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>    
        <div class="container">
            <div class="row">
                <div class="col-md-5">
                    <div class="vision">
                        <h3 class="text-center">Our Vision</h3>
                        <p>Built a big opportunity group for youth and make youth dream big about get employed
                            and reduce a number of unemployed people in the country through this application,
                            we also have vision of removing much movement for work finding and time lossing.
                            To acheive this we need you that's why this is yours.
                        </p>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="mission">
                        <h3 class="text-center">Our Mission</h3>
                        <p>Our main mission is to "facilitate the communication between job seekers and employers 
                            operating in Rwanda,Africa using online technologies". On top of this daily activity,
                            we organise an annual career day event, where the two stakeholders meet in the same place for one-to-one interviews.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="m-auto col-md-5">
                    <div class="services">
                        <h3 class="text-center">Our Services</h3>
                        <p> <b>Job Opportunity: </b>As you are here the first thing we do is giving job opportunities. <br> 
                            <b>Online Enterview: </b>We connect user and admins through out interview app. <br>
                            <b>Online Joining: </b>We connect user and admin to be together. <br>
                            <b>App Storing: </b>In our app there is part of app store we help you to store your app. <br>
                            <b>Bussiness Studing: </b>We can teach you how to start and run your bussiness while with us. </p>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="col-md-5">
                    <div class="about-aptlab">
                        <h3 class="text-center">About OPTLab</h3>
                        <p>S
                        </p>
                    </div>
                </div> -->
        <!-- <img src="images/vision.png" alt=""> -->
    </div>



        <div class="back-top">
            <a class="back-to-top" href="#page-top"><i class="fas fa-chevron-up"></i></a>
        </div>
</body>
<script src="javascript/index.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="Bootstrap/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/e5644a9822.js" crossorigin="anonymous"></script>
</html>