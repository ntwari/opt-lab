<?php 
    include("dbconfig.php");
?>

<?php
function timeAgo($time_ago){

			$time_ago = strtotime($time_ago);
			$cur_time   = time();
			$time_elapsed   = $cur_time - $time_ago;
			$seconds    = $time_elapsed ;
			$minutes    = round($time_elapsed / 60 );
			$hours      = round($time_elapsed / 3600);
			$days       = round($time_elapsed / 86400 );
			$weeks      = round($time_elapsed / 604800);
			$months     = round($time_elapsed / 2600640 );
			$years      = round($time_elapsed / 31207680 );
			// Seconds
			if($seconds <= 60){
			    return "just now";
			}
			//Minutes
			else if($minutes <=60){
			    if($minutes==1){
			        return "one minute ago";
			    }
			    else{
			        return "$minutes minutes ago";
			    }
			}
			//Hours
			else if($hours <=24){
			    if($hours==1){
			        return "an hour ago";
			    }else{
			        return "$hours hrs ago";
			    }
			}
			//Days
			else if($days <= 7){
			    if($days==1){
			        return "yesterday";
			    }else{
			        return "$days days ago";
			    }
			}
			//Weeks
			else if($weeks <= 4.3){
			    if($weeks==1){
			        return "a week ago";
			    }else{
			        return "$weeks weeks ago";
			    }
			}
			//Months
			else if($months <=12){
			    if($months==1){
			        return "a month ago";
			    }else{
			        return "$months months ago";
			    }
			}
			//Years
			else{
			    if($years==1){
			        return "one year ago";
			    }else{
			        return "$years years ago";
			    }
			}
		} 
?>
<?php 
  if(isset($_POST["btn-login"]))
  {
      $email=$_POST["txt_uname_email"];
      $pass=$_POST["txt_password"];
      
      $sql=mysqli_query($con,"select * from registration where email='$email' and password='$pass'");
      if(mysqli_num_rows($sql))
      {
          while($row=mysqli_fetch_array($sql))
          {   
              $name=$row["name"];
              $id=$row["usr_id"];
              $image=$row["image"];
              session_start();
              $_SESSION["name"]=$name;
              $_SESSION["id"]=$id;
              $_SESSION["email"]=$email;
              $_SESSION["image"]=$image;
              
          }
        header("location:user.php");
      }
      else{
         //$error="";
         echo '<script>alert("plz inter valid password");</script>';
      }
  }
?>
<?php
if(isset($_POST['btn-signup']))
{
	$uname = strip_tags($_POST['txt_uname']);
	$umail = strip_tags($_POST['txt_umail']);
	$upass = strip_tags($_POST['txt_upass']);
	$pic=$_FILES["img"]["name"];
    $tmp=$_FILES["img"]["tmp_name"];
    $type=$_FILES["img"]["type"];
   
      
    $path="user_images/".$pic;
	$icon="warning";
	$class="danger";
	
	if($uname=="")	{
		$error[] = "provide username !";	
	}
	else if($type=="application/pdf" || $type=="application/pdf" || $type=="application/x-zip-compressed")	{
		$error[] = "this type of file does not supported !";
		echo '<script>alert("this type of file does not supported !");</script>';	
	}
	else if($pic=="")	{
		$error[] = "Select Image !";	
	}
	else if($umail=="")	{
		$error[] = "provide email id !";	
	}
	else if(!filter_var($umail, FILTER_VALIDATE_EMAIL))	{
	    $error[] = 'Please enter a valid email address !';
	}
	else if($upass=="")	{
		$error[] = "provide password !";
	}

	else
	{
		//$sql="insert into registration values();"
		$sql= mysqli_query($con,"insert into registration(name,email,image,password) values('$uname','$umail','$pic','$upass')");
		if($sql)
		{  
            move_uploaded_file($tmp,$path);
		   $error[] = "Registration successfully for login click on sign button";
		   echo '<script>alert("Registration successfully for login click on sign button");</script>';
		   $icon="success";
	       $class="success";	
		}
	}	
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Work Scout</title>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<link rel="stylesheet" href="css1/style.css">
<link href="https://fonts.googleapis.com/css?family=Dosis" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="css1/colors/green.css" id="colors">
<link rel="stylesheet"
href="https://fonts.googleapis.com/css?family=Tangerine">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link href="https://fonts.googleapis.com/css?family=Patua+One|Teko&display=swap" rel="stylesheet">
<script src="https://kit.fontawesome.com/yourcode.js"></script>
</head>
<style type="text/css">
input {
   width: 32em !important;
}
.modal{
	margin-left: 35em;
	display: none;
	margin-bottom: 2em;
}
h4{
	margin-bottom: 3em;
}
button{
	background: linear-gradient(#e67e22,#e67e22,#f39c12,#e67e22,#e67e22);
}
.btn-primary{
	background: linear-gradient(#e67e22,#e67e22,#f39c12,#e67e22,#e67e22);
}
.post a{
	background: linear-gradient(#e67e22,#e67e22,#f39c12,#e67e22,#e67e22);
}
.text-center{
	color: #e67e22;
	font-size: 1.4em;
	font-weight: 900;
	border-bottom: 1px solid #e67e22;
}
#footer{
	background: linear-gradient(#e67e22,#e67e22,#f39c12,#e67e22,#e67e22);
}
body{
    font-family: 'Teko', sans-serif;
    font-weight: 100;
    word-spacing: 5px;
    font-size: 1.6em;
    line-height: 30px;
}
.btn-primary{
	margin-bottom: 2em;
}
.contents:before{
	font-family: 'Tangerine',serif;
	content: " ";
	animation-name: contentsMoving;
	animation-duration: 15s;
	animation-delay: 5s;
	text-align: center;
	color: #f39c12;
	font-size: 2em;
	transition: ease-in-out 1s;
	margin-left: 5em;
	animation-iteration-count: infinite;
}
.announce{
	animation-name: stunning;
	animation-duration: 4s;
}
@keyframes contentsMoving{
	0%{content: "Universal seek Job"}
	20%{content: "we`re here to assist..."}
    40%{content: "employers,companies and ..."}
    60%{content: "employees !!!!"}
    70%{content: "we are ........."}
    100%{content: "Universal canvas";}
}
@keyframes stunning{
	0%{display: none;}
	6%{display: none;}
	70%{margin-left: 2em;}
	80%{margin-right: 2em}
    90%{margin-right: 0px;}
    100%{margin-bottom: 0em;}
}
#container{
	background-color: #34495e;
	border: none;
	border-bottom: 20px solid #2c3e50;
}
.header{
    position: sticky;
	top: 0;
}
.someContainers{
	background-color: #bdc3c7;
	padding: 2em 3em;
}
.someContainers div{
	display: inline-block;
}
.someContainers h1{
   font-family: sans-serif;
   color: #2c3e50;
   text-align: center;
   padding: 1em;
   font-weight: 900;
   font-size: 1.6em;
}
.smallContainers{
	background-color: #ffff;
	border: none;
	border-radius: 0.6em;
	margin-right: 7em;
	width: 30%;
    padding: 1em;
    font-family: sans-serif;
    font-weight: 100;
    color: #7f8c8d;
    box-shadow: 6px 15px 15px #7f8c8d;
}
.smallContainers h2{
    font-family: 'Teko';
    color: #e67e22;
    text-align: center;
    font-size: 1.4em;
}
.smallContainers button:hover{
	box-shadow: 2px 3px 3px #7f8c8d;
}
.smallContainers strong{
	color: #34495e;
}
</style>
<body>
<div class="header1">
	<div class="headerSection">
		
	</div>
	<div class="headerSection">
		
	</div>
<div id="wrapper">
<header class="transparent sticky-header full-width">
<div  class="header">	
<div class="container" id="container">
	<div class="sixteen columns">
	
		<!-- Logo -->
		<div id="logo" style="margin-top:15px">
			<h1><a href="index.php"><font style="font-size:2em; color:white;font-family: 'Tangerine', serif;">universal Seek job</font></a></h1>
		</div>

		<!-- Menu -->
		<div class="menuToBeStyled">
		<nav id="navigation" class="menu">
			<ul id="responsive">
				<li class="post"><a data-toggle = "modal" data-target = "#myModal" id="current">Post</a>
				</li>
			</ul>
			<ul class="float-right">
				
				<li class="signs"><a data-toggle = "modal" data-target = "#signupModal"><i class="fa fa-user" ></i> Sign Up</a></li>
				<li class="logins"><a data-toggle = "modal" data-target = "#myModal"><i class="fa fa-lock"></i> Log In</a></li>
			</ul>
		</nav>
		</div>
		<!-- Navigation -->
		<div id="mobile-navigation">
			<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
    </div>
	</div>
</div>
</div>
</header>
<div class="clearfix"></div>
<div id="banner" class="with-transparent-header parallax background" style="background-image: url(images/banner-home-02.jpg)" data-img-width="2000" data-img-height="1330" data-diff="300">
	<div class="container">
		<div class="sixteen columns">
			
			<div class="search-container">
                <div class="contents">
                	
                </div>
				<div class="announce" style="font-size: 1.5em;">
					We’ve over <strong style="color: #e67e22;font-size: 2em;">15 000</strong> Post offers for you!
				</div>

			</div>

		</div>
	</div>
</div>
<section class="signing">
<div class = "modal fade" id = "myModal" tabindex = "-1" role = "dialog" 
   aria-labelledby = "myModalLabel" aria-hidden = "true">
   
   <div class = "modal-dialog">
      <div class = "modal-content">
         
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            
            <h4 class = "modal-title" id = "myModalLabel">
               Login now to your portal
            </h4>
         </div>
         <form class="form-signin" method="post" id="login-form">
         
         <div class = "modal-body">
            <input type="email" name="txt_uname_email" placeholder="Enter Your email here" />
         </div>
         <div class = "modal-body">
            <input type="password" name="txt_password" placeholder="Enter Your password" />
         </div>
         
         <div class = "modal-footer">
            
            <input type = "submit" class = "btn btn-primary" name="btn-login" value="LOG IN">
               
            </input>
			<button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
            
        </form>
         </div>
         
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
  
</div><!-- /.modal -->
</section>
<!-- Modal for login -->
<section class="login">
<div class = "modal fade" id = "signupModal" tabindex = "-1" role = "dialog" 
   aria-labelledby = "myModalLabel" aria-hidden = "true">
   
   <div class = "modal-dialog">
      <div class = "modal-content">
         
         <div class = "modal-header">
            <button type = "button" class = "close" data-dismiss = "modal" aria-hidden = "true">
                  &times;
            </button>
            
            <h4 class = "modal-title" id = "myModalLabel">
               Sign in portal
            </h4>
            <form method="post" class="form-signin" enctype="multipart/form-data">
         </div>
         <div class = "modal-body">
            Name: <input type="text" name="txt_uname" placeholder="Enter Your name" required/>
         </div>
         
         <div class = "modal-body">
            Email : <input type="email" name="txt_umail" placeholder="Enter Your email here" required/>
         </div>
         <div class = "modal-body">
            Password: <input type="password" name="txt_upass" placeholder="Enter Your password" required/>
         </div>
         <div class = "modal-body">
            Slect Your Photo: <input type="file" name="img" required/>
         </div>
         
         <div class = "modal-footer">
            
            <input type = "submit" class = "btn btn-primary" value="SIGN UP" name="btn-signup">
			 <button type = "button" class = "btn btn-default" data-dismiss = "modal">
               Close
            </button>
           
		   
            </input>
         </div>
         </form>
      </div><!-- /.modal-content -->
   </div><!-- /.modal-dialog -->
  
</div><!-- /.modal -->
</section>
<div class="container">
	<!-- Recent Jobs -->

	<h1 class="text-center">Newly Posted Job</h1>
	<div class="eleven columns">
	<div class="padding-right">
		<ul class="job-list full">
			<?php 
		   $query="select * from posts join registration where posts.usr_id_p=registration.usr_id order by posts.status_time desc limit 2";
           $sql=mysqli_query($con,$query);
                             while($row=mysqli_fetch_array($sql))
                               {
                                        $title=$row["status_title"];
                                        $status=$row["status"];
                                        $img=$row["image"];
                                        $name=$row["name"];
                                        $time=$row["name"];
                                        $time_ago=$row["status_time"];
                                      echo '<li><a href="#">';
				                          echo '<img src="user_images/'.$img.'" alt="">';
				                             echo '<div class="job-list-content">';
					                           echo '<h4>'.$title.'</h4>';
					                            
					                           echo '</div>';
					                           echo '<p>'.$status.'</p><br>';
					                           echo '<div class="job-icons" style="margin-left:100px">';
					                            	echo '<span><i class=""></i><strong>Posted by:<strong> '.$name.'</span><br><br>';
						                           echo '<span><i class=""></i> </span><br>';
						                          echo '<span><i class=""></i><font style="color:blue">'.timeAgo($time_ago).'</font></span><br>';
				                               echo '</div>';
				                                echo '</a>';
				                          echo '<a data-toggle = "modal" data-target = "#myModal" class="btn btn-default" >comment</a>';
				                          echo '<br/><br/>';
			                            echo '</li>';
			                        }
           ?>
			
		</ul>
		<div class="clearfix"></div>

		<div class="pagination-container">
		</div>
	</div>
	</div>
	<div class="five columns">
          
		<!-- Sort by -->
		<div class="widget">
            
			<img src="images/post_a_job.jpg" height="100px" width="100px"/>


		</div>

		<!-- Location -->
		<div class="widget">
			
			
		</div>

		<!-- Job Type -->
		<div class="widget">
			<h2>Find Jobs & Post Jobs</h2>

			<ul class="checkboxes">
				<li>
					<img src="images/fb-jobs.png" height="100px" width="100px"/>
				</li>
			</ul>
		</div>
	</div>
	<!-- Widgets / End -->
</div>
<section class="someContainers">
	<h1>What you can get here </h1>
	<div class="smallContainers">
		<div>
			<i class="fa fa-user-hard-hat"></i>
			<h2 style="margin-bottom: 1em;">Get worker</h2>
			<p>Here you can <strong>request for project </strong> any time.</p>
			<p>We provide <strong>professional mail box</strong> any time.</p>
			<p>Then we provide <strong>room for discussing </strong> about project.</p>
			<p>We use <strong>agenda storing </strong> to store your profile.</p>
			<p>Here you can <strong>request for project </strong> any time.</p>
			<button style="border: none;border-radius: 8px;background-color: #e67e22;color: #fff;margin-top: 2em;">Get Projects </button>
		</div>
	</div>
	<div class="smallContainers">
		<div>
			<i></i>
			<h2 style="margin-bottom: 1em;">Get Job</h2>
			<p>Here you can <strong>request for project </strong> any time.</p>
			<p>We provide <strong>professional mail box</strong> any time.</p>
			<p>Then we provide <strong>room for discussing </strong> about project.</p>
			<p>We use <strong>agenda storing </strong> to store your profile.</p>
			<p>Here you can <strong>request for project </strong> any time.</p>
			<button style="border: none;border-radius: 8px;background-color: #e67e22;color: #fff;margin-top: 2em;">Get Projects </button>
		</div>
	</div>
</section>
<div style="display: flex;flex-direction: row;">
	<img src="images/welcome.png">
<div>
	<ul class="social-icons">
		<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
		<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
		<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
		<li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
	</ul>
	<div style="background-color: #95a5a6;border: none;border-radius: 0.6em;box-shadow: 4px 9px 9px #7f8c8d;color: #2c3e50;padding: 1em; margin-top: 4em;">
		Hello please login to start to use our app <br><br>
		we think it will be helpful
	</div>
</div>
<div  style="margin-left: 4em;margin-top: 4em;">
	<h4 class="hover">Hover for more links</h4>
	<div>
		<ul style="font-family: sans-serif;font-weight: 100;font-size: 0.7em;margin-top: -2em;cursor: pointer;color: #95a5a6;border: none;border-radius: 0.5em;box-shadow: 1px 3px 4px 5px #7f8c8d;padding: 1em;display: none;" class="seeme">
			<li>Home </li><br>
			<li>Login </li><br>
			<li>Sign up </li><br>
		</ul>
	</div>
</div>
<div style="width: 30%" class="ftr">
	<img src="images/employees.jpg">
	<div style="font-family: sans-serif;font-weight: 100;font-size: 0.8em;margin-top: 2em;display: none;margin-left: 2em;border-bottom: 2px solid #7f8c8d">
		hello we all also got the job throught <strong style="font-family: 'Tangerine', serif;">universal Seek Job</strong><br> This will work for you and your inheritance.
	</div>
</div>
</div>

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div>	
</div>
<script type="text/javascript">
	$(document).ready(function(){
      $(".logins").click(function(){
   			$("#myModal").slideDown(1500);
   			$("#wrapper").css('position','sticky');
   			$("#wrapper").css('top',0);
   		});
      $(".close").click(function(){
      	$(".close").css("background-color","#34495e");
      	$("#myModal").slideUp(1500);
      })
      $(".signs").click(function(){
   			$("#signupModal").slideDown(1500);
   		});
      $(".close").click(function(){
      	$("#signupModal").slideUp(1500);
      })
      $(".post").click(function(){
      	$("#myModal").slideDown(1500);
      })
      $(".hover").mouseenter(function(){
      	$('.seeme').slideToggle(100);
      })
      $(".seeme li").mouseenter(function(){
      	$(".seeme li").css("color","#34495e");
      })
      $(".seeme li").click(function(){
      		$("#myModal").slideDown(1500);
   			$("#wrapper").css('position','sticky');
   			$("#wrapper").css('top',0);
   			confirm("please on the top their is the login page opened....")
      })
      $(".ftr img").mouseenter(function(){
      	$(".ftr div").slideToggle(1000);
      })
	});
</script>
</body>
</html>