<?php
include("dbconfig_admin.php");
if(isset($_GET['email']) && !empty($_GET['email']) AND isset($_GET['hash']) && !empty($_GET['hash'])){
    $email =$_GET['email'];
    $hash =$_GET['hash'];
    $sql="SELECT * FROM admins WHERE email=:email AND hash=:hash AND active='0'";
    $query=$connection->prepare($sql);
    $query->bindParam(":email",$email);
    $query->bindParam(":hash",$hash);
    $query->execute(); 
    $num_of_rows = $query->fetchColumn(); 
    if($num_of_rows>0){
      $rows = $query->fetch();
      if(isset($_POST['first']) && isset($_POST['last']) && isset($_POST["myfile"])){
        $first_name=$_POST["first"];
        $last_name=$_POST["last"];
        $phone=$_POST["phone"];
        $password=$_POST["password"];
        $active=1;
        $national_id=$_POST["national_id"];
        $temporary_filename = $_FILES['myfile']['tmp_name'];
        $final_filename =  $_FILES['myfile']['name'];
        $file_type =  $_FILES['myfile']['type'];
        move_uploaded_file($temporary_filename, "user_images/" . $final_filename);

        $sql1="UPDATE admins SET admin_first_name=:first_name,admin_last_name=:last_name,phone=:phone,profile=:file_uploaded,national_id=:national,password=:pass,active=:active WHERE email=:email AND hash=:hash";
        $query1=$connection->prepare($sql1);
        $query1->bindParam(":first_name",$first_name);
        $query1->bindParam(":last_name",$last_name);
        $query1->bindParam(":phone",$phone);
        $query1->bindParam(":file_uploaded",$final_filename);
        $query1->bindParam(":national",$national_id);
        $query1->bindParam(":pass",$password);
        $query1->bindParam(":active",$active);
        $query1->bindParam(":email",$email);
        $query1->bindParam(":hash",$hash);

        if($query1->execute()){
            $msg=$temporary_filename;
        }
        else{
          $msg="THE DATA NOT STORED IN THE SYSTEM";
        }
      }
      else if(isset($_POST['company']) && isset($_POST['num_workers'])){
        $company_name=$_POST["company"];
        $company_task=$_POST["company_desc"];
        $number_of_workers=$_POST["num_workers"];
        $ceo=$rows["admin_id"];
        $website=$_POST["website"];
        $sql2="INSERT INTO company(comp_name,comp_task,num_of_workers,CEO,Website) VALUES (:comp_name,:comp_task,:workers,:ceo,:website)";
        $query2=$connection->prepare($sql2);
        $query2->bindParam(":comp_name",$company_name);
        $query2->bindParam(":comp_task",$company_task);
        $query2->bindParam(":workers",$number_of_workers);
        $query2->bindParam(":website",$website);
        $query2->bindParam(":ceo",$ceo);
        if($query2->execute()){

        }
      }
   }
   else{
    $msg="NOT VERIFIED";
  }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head>
        <title>ADMIN SIGNUP</title>
        <link rel="stylesheet" href="css1/admin-style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css?family=Saira+Condensed&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
   </head>
   <body>
       <section class="navbar">
       <h1><i class="fa fa-signal-stream"></i><span>OPT</span>Lab</h1>
       <ul>
           <li><a href="#">Home</a></li>
           <li><a href="#">About us</a></li>
           <li><a href="#">Login</a></li>
           <li><a href="#" id="actions">Sign up</a></li>
       </ul>
       </section>
       <section class="body">
       <section class="sidebar"  style="height:1000px;">
       </section>
       <section class="admin-form" style="height:1000px;">
       <section id="admin-form">
            <div class="headers">
                 <h1>ADMIN SIGNUP FORM</h1>
            </div>
            <?php 
          if(isset($msg)){
              echo '<div class="statusmsg">'.$msg.'</div>'; 
           } 
          ?>
            <div class="description">
                 <p>You have confirmed the email  now continue in signup,by filling this below form:</p>
            </div>
            <form action="#" method="post">
            <div class="inputWithIcon">
            <i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i>
             <input type="text" placeholder="Enter your first name" name="first" required>
            </div>
            <div class="inputWithIcon">
            <i class="fa fa-user fa-lg fa-fw" aria-hidden="true"></i>
             <input type="text" placeholder="Enter your last name" name="last" required>
            </div>

            <div class="inputWithIcon">
            <i class="fa fa-phone fa-lg fa-fw" aria-hidden="true"></i>
             <input type="number" placeholder="Phone Number" name="phone" required>
            </div>

            <div class="inputWithIcon inputIconBg">
            <i class="fas fa-key" aria-hidden="true"></i>
              <input type="password" placeholder="Enter password to use" name="password" required>
            </div>
            <div class="inputWithIcon inputIconBg">
            <i class="fas fa-key" aria-hidden="true"></i>
              <input type="password" placeholder="confirm password" name="confirm-password" required>
            </div>

            <div class="inputWithIcon inputIconBg">
            <i class="fas fa-globe-asia"></i>
             <input type="number" placeholder="Enter your national id" name="national_id" required>
            </div>
            <div class="inputWithIcon inputIconBg">
            <i class="fas fa-id-badge"></i>
             <input type="file" name="myfile" accept="image/*" required>
            </div>
            <input type="submit" class="submit" value="Next Step" onclick="showCompany()">
            </form>
            </section>


            <section id="company"  style="display: none">
            <div class="headers">
                 <h1>REGISTER COMPANY</h1>
            </div>
            <?php 
          if(isset($msg)){
              echo '<div class="statusmsg">'.$msg.'</div>'; 
           } 
          ?>
            <div class="description">
                 <p>As you have finished to register the user now you are going to fill this company recording form as this one will help you to post the work on the dashboard as it should be posted by Company CEO as you will insert him:</p>
            </div>
            <form action="#" method="post">
            <div class="inputWithIcon">
            <i class="fas fa-file-signature"></i>
             <input type="text" placeholder="Enter company name" name="company" required>
            </div>

            <div class="inputWithIcon">
            <i class="fas fa-user-friends"></i>
             <input type="number" placeholder="Number of workers" name="num_workers" required>
            </div>
            
            <div class="inputWithIcon">
            <i class="fab fa-superpowers"></i>
             <input type="text" placeholder="Enter website" name="website" required>
            </div>

            <div class="inputWithIcon inputIconBg">
              <input type="hidden" value="<?php echo $admin_id?>" name="admin_id" required>
            </div>

            <div class="inputWithIcon inputIconBg">
             <textarea name="comp-task" id="company_desc" cols="60" placeholder="What do you do?" rows="4"></textarea>
             </div>
            <input type="submit" class="submit" value="complete" onclick="showCompany()">
            </form>
            </section>
       </section>
           <!-- THE FORM OF REGISTERING THE COMPANY -->
       </section>

       <script>
          function showCompany(){
            document.getElementById("admin-form").style.display="none";
            document.getElementById("company").style.display="block";
            }
       </script>
   </body>
</html>