<?php 
    include("../includes/dbconfig.php");
?>
<?php
  session_start();
  if($_SESSION['name']=='')
  {
     header('location:reg.php');
  }

 ?>
<?php
function timeAgo($time_ago){

      $time_ago = strtotime($time_ago);
      $cur_time   = time();
      $time_elapsed   = $cur_time - $time_ago;
      $seconds    = $time_elapsed ;
      $minutes    = round($time_elapsed / 60 );
      $hours      = round($time_elapsed / 3600);
      $days       = round($time_elapsed / 86400 );
      $weeks      = round($time_elapsed / 604800);
      $months     = round($time_elapsed / 2600640 );
      $years      = round($time_elapsed / 31207680 );
      // Seconds
      if($seconds <= 60){
          return "just now";
      }
      //Minutes
      else if($minutes <=60){
          if($minutes==1){
              return "one minute ago";
          }
          else{
              return "$minutes minutes ago";
          }
      }
      //Hours
      else if($hours <=24){
          if($hours==1){
              return "an hour ago";
          }else{
              return "$hours hrs ago";
          }
      }
      //Days
      else if($days <= 7){
          if($days==1){
              return "yesterday";
          }else{
              return "$days days ago";
          }
      }
      //Weeks
      else if($weeks <= 4.3){
          if($weeks==1){
              return "a week ago";
          }else{
              return "$weeks weeks ago";
          }
      }
      //Months
      else if($months <=12){
          if($months==1){
              return "a month ago";
          }else{
              return "$months months ago";
          }
      }
      //Years
      else{
          if($years==1){
              return "one year ago";
          }else{
              return "$years years ago";
          }
      }
    } 
?>
<?php 
$sql="SELECT * FROM `registration` WHERE `name`='".$_SESSION['name']."'";
  $result = $con->query($sql);
      if ($result->num_rows>0)
         {    
          while($row = $result->fetch_assoc()) 
          {     
        $image=$row['image'];
}
}
 ?>
 <!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="components/bootstrap/dist/js/jquery.js"></script>
 <link href="components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
 <!-- Custom styles for this template -->
 <link href="components/bootstrap/dist/css/simple-sidebar.css" rel="stylesheet">
  <link href="components/bootstrap/dist/css/postmodal.css" rel="stylesheet">
  <link href="components/bootstrap/dist/css/fbbox.css" rel="stylesheet">
   <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Tangerine">
<link href="https://fonts.googleapis.com/css?family=Teko&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Patua+One|Teko&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/css?family=Barlow+Condensed&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Khand|Palanquin|Viga|Yantramanav&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<style type="text/css">
	body{ font-family: Dosis !important; font-size: 1.3em; line-height: 40px;background: #E5E5E5;}
	.navbar{
		background: #34495e;border-bottom: 1em solid #2980b9;color: #ffff;height: 90px;}
	.toggled{
		background-color: #34495e;}
	#menu-toggle:hover{
       background-color: #7f8c8d;height: 100%;width: 120%;border: none;border-radius: 1em;}
	#sidebar-wrapper{background-color: #ecf0f1;	margin-top: 3em;border-right: 12px solid #27ae60;font-family:'Khand', sans-serif;font-size: 1.3em;font-weight: 100;}
	.list-group a:hover{background-color: #34495e;   }
    img{     height: 50px;   border: none;    border-radius: 50%;   }
    .post-show{   	box-shadow: 2px 4px 4px #7f8c8d;  }
    .post-show:hover{	box-shadow: 5px 13px 13px #7f8c8d; }
    .post-show .btn-default{background-color: #e67e22;color: #fff;height: 3em;text-align: center;font-family: 'roboto';}
    .post-show .btn-defaul:hover{	box-shadow: 2px 4px 4px #7f8c8d;background-color: #7f8c8d; } 
    .post-show .apply{color: #fff;}
    .post-show .aplly{
       background-color: #7f8c8d;width: 20%;text-align: center;border: none;border-radius: 10px;float: right; font-family: 'Teko',sans-serif}
    .getIn{font-family: 'Teko',sans-serif;font-size: 4em;	margin-top: 2em;text-align: center;color: #e67e22;}
    .pages a{color: #fff;text-align: center;}
	    .pages{	background-color:#2c3e50;font-size: 1em;margin-bottom: 2em;	height: 2em;width: 3em;	float: left;text-align: center;border: none;border-radius: 30%;margin-right: 5em;}
    .pages:hover{box-shadow: 2px 4px 4px #7f8c8d;}
    .likebutton:hover{transform: rotate(30deg);cursor: pointer;transition: ease-in-out 0.8s;}
	.img{margin-left: 97em;margin-top: -8em}
	.side-nav{margin-top: 5em;}
	.sidebar-nav li a{background: #34495e;font-family:'Khand', sans-serif;margin-top: 1em}
	.profile img{margin-top: 3em;margin-left: 2em;height: 120px;width:  120px;border: 1px solid #27ae60;}
	.name{text-align: center;font-family:'Khand', sans-serif;color: #e67e22;margin-left: -1em;font-size: 1.4em;}
	.links ul li{background: #34495e;margin-top: 1em;cursor: pointer}
	.links ul li:hover{background: #2ecc71;transition: ease-in-out 0.5s;}
	.links ul li a{text-decoration: none;font-family:'roboto';margin-left: 2em;padding-bottom: 0.4em;}
    .links ul li a i{color: white;font-size: 2em;padding-right: 0.4em;padding-top: 0.4em}
    /* other for the comment php */

    .holder{position: absolute;margin-top: 3em;height: 130px;width: 723px;background: #C4C4C4;box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25);backdrop-filter: blur(4px);}
    .users-img{position: absolute;width: 113px;height: 112px;border: none;border-radius: 50%}
    .name{position: absolute;
width: 137px;height: 36px;font-style: normal;font-weight: normal;font-size: 23px;line-height: 36px;color: #EDF7FA;}
.discr{margin-top: 2em;font-style: normal;font-weight: normal;font-size: 19px;line-height: 36px;color: black;margin-left: 7em;}
.holder2{width: 723px;height: 289px;background: #C4C4C4;box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25), 0px 4px 4px rgba(0, 0, 0, 0.25);margin-top: 8em;font-size: 2em}
textarea{height: 184px;background: #FFFDFD;margin: 2em;font-family:'Khand', sans-serif;font-size: 1.5em;}
.button{width: 207px;height: 39px;background: #34495E;box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);border: none;border-radius: 20px/12px;color: #fff;margin-left: 4em}
.button:hover{background: #7f8c8d;transition: ease-in-out 0.4s;}
</style>
<body>
 <div class="col-lg-8" style="margin-top: 7em;margin-left: 13em;">
  <?php
$title = $_GET['s_title'];
$p_id = $_GET['id'];
$sql1=mysqli_query($con,"select * from posts where post_id='$p_id'");
                    while($comnt1=mysqli_fetch_array($sql1)){
                           $uid=$comnt1["usr_id_p"];
                        }
                        $sql2=mysqli_query($con,"select * from admins where admin_id='$uid'");
                    while($comnt2=mysqli_fetch_array($sql2)){
                           $im=$comnt2["image"];
                           $name=$comnt2["name"];
                        }
?>      
<div class="holder">
   <div class="img-holder">
   <img src="../user_images/<?php echo $im; ?>" class="users-img" alt="user profile image">
   </div>
   <div style="margin-left: 10em">
   <p class="name">Posted by <?php echo $name; ?></p>
   </div>
   <p class="discr"><?php echo $title;?></p>
</div>
<div class="holder2">
<form action="comment.php" method="post">
            <div class="post-footer">
                <div class="input-group"> 
                    <textarea class="form-control" cols="50" rows="8" placeholder="Add a comment" type="text" name="comment"></textarea>
                    <input class="form-control"  type="hidden" name="uid" value="<?php echo $_SESSION['id']; ?>">
                    <input class="form-control"  type="hidden" name="pid" value="<?php echo $p_id;?>">
                    <div class="btn-group" role="group" style="margin-top:5px">
                    <button type="submit" name="submit_comment" class="button">Comment  </button>
                </div>
                </div>
                </form>
</div>
<script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
<script>
 $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.status').click(function() { $('.arrow').css("left", 0);});
            $('.photos').click(function() { $('.arrow').css("left", 146);});
        });
    </script>
    

</body>
</html>