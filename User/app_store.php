<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home | App Store</title>
    <link rel="stylesheet" href="styles/app_store.css">

</head>
<body>
    <?php include '../includes/user_info.php'; ?>
    <nav class="nav">
        <div class="logo">
          <a href="../index.php">
            <p><span>OPT</span>Lab</p>
          </a>
        </div>
      <div class="menu">
        <ul class="menu-pages">
          <li> <a href="home.php" class="active">Home</a> </li>
          <li> <a href="#">About Us</a> </li>
          <li> <a href="#">Help</a> </li>
        </ul>
        <div class="header_profile">
          <?php echo "<img src='user_images/".$_SESSION["profile"]."' >";?>
        </div>
        <ul class="log-out">
          <li> <a href="sign-out.php">Log Out</a> </li>
        </ul>
      </div>
    </nav>
<div class="main-user-dash">
        <div class="user-services">
          <div class="user-profile">
            <?php echo "<img src='user_images/".$_SESSION["profile"]."' >";?>
          </div>
          <div class="service-part">
            <ul>
              <li><a href="home.php"><i class="fa fa-user" aria-hidden="true"></i>Account overview</a></li>
              <li ><a href="#"><i class="fa fa-home" aria-hidden="true"></i>Dashboard</a></li>
              <li><a href="#"><i class="fa fa-paint-brush" aria-hidden="true"></i>Edit profile</a></li>
              <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i>Your applications</a></li>
              <li><a href="#"><i class="fa fa-eye" aria-hidden="true"></i>Available companies</a></li>
              <li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a></li>
              <li class="active-service"><a href="app_store.php"><i class="fa fa-puzzle-piece" aria-hidden="true"></i>Apps</a></li>
              <li><a href="#"><i class="fa fa-history" aria-hidden="true"></i>History</a></li>
              <li><a href="#"><i class="fa fa-trash" aria-hidden="true"></i>Deleted Data</a></li>
              <li><a href="sign-out.php"><i class="fa fa-sign-out-alt" aria-hidden="true"></i>Log Out</a></li>
            </ul>
          </div>
        </div>
        <div class="about-service">
            <div class="apps">
                <h2>Find your Favorite app here</h2>
                <div class="container">
                      <h3>Most rated Apps</h3>
                    <div class="row">
                        <div class="col-md-2">
                            <a href="https://www.google.com/chrome/?brand=CHBD&gclid=CjwKCAjw3c_tBRA4EiwAICs8Cks_Z6CPmL1XQNHzdW7W81lZyl1Mppjqm77r33wTQ_esse-MksHUVxoCWUEQAvD_BwE&gclsrc=aw.ds">
                                <img src="app-images/chrome-logo.png" alt="chrome-logo">
                            </a>
                                <div class="rating">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half"></i>
                                    <i class="far fa-star"></i>
                                </div>
                        </div>
                        <div class="col-md-2">
                            <a href="https://chromium.woolyss.com/download/en/">
                                <img src="app-images/chromium-logo.png" alt="chrome-logo">
                            </a>
                                <div class="rating">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half"></i>
                                    <i class="far fa-star"></i>
                                </div>
                        </div>
                        <div class="col-md-2">
                            <a href="https://noxofficial.com/nox-for-pc/">
                                <img src="app-images/nox-logo.jpeg" alt="chrome-logo">
                            </a>
                                <div class="rating">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half"></i>
                                    <i class="far fa-star"></i>
                                </div>
                        </div>
                        <div class="col-md-2">
                            <a href="https://play.google.com/store/apps?utm_source=na_Med&utm_medium=hasem&utm_content=Jan0219&utm_campaign=Evergreen&pcampaignid=MKT-DR-na-us-1000189-Med-hasem-ap-Evergreen-Jan0219-Text_Search_SKWS-id_100474_%7cPHR%7cONSEM_kwid_43700019962491538&gclid=CjwKCAjw3c_tBRA4EiwAICs8Ci8fFVINn_TuXL-MUKdhf3l_JId0D4IpKr1f0gK2Qn_FAlECx4RdqhoCYcMQAvD_BwE&gclsrc=aw.ds">
                                <img src="app-images/appstore-logo.jpg" alt="chrome-logo">
                            </a>
                                <div class="rating">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half"></i>
                                    <i class="far fa-star"></i>
                                </div>
                        </div>
                        <div class="col-md-2">
                            <a href="https://www.spotify.com/us/premium/?utm_source=us-en_brand_contextual_text&utm_medium=paidsearch&utm_campaign=alwayson_ucanz_us_performancemarketing_lowsubintent_brand+contextual+text+exact+us-en+google&gclid=CjwKCAjw3c_tBRA4EiwAICs8ClwEYiyVdDCo8q3ptQpQmNZSp80K8f57sLWf3fNn8iJc97NGzQ69qhoCqCIQAvD_BwE&gclsrc=aw.ds">
                                <img src="app-images/spotify-logo.png" alt="chrome-logo">
                            </a>
                                <div class="rating">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half"></i>
                                    <i class="far fa-star"></i>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</body><link rel="stylesheet" href="../Bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="../Bootstrap/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/e5644a9822.js" crossorigin="anonymous"></script>
</html>