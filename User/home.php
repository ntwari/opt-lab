<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Home</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/home.css">
  </head>
  <body>
    <nav class="nav">
        <div class="logo">
          <a href="../index.php">
            <p><span>OPT</span>Lab</p>
          </a>
        </div>
      <div class="menu">
        <ul class="menu-pages">
          <li> <a href="home.php" class="active">Home</a> </li>
          <li> <a href="#">About Us</a> </li>
          <li> <a href="#">Help</a> </li>
        </ul>
        <div class="header_profile">
          <?php echo "<img src='user_images/".$_SESSION['profile']."' >";?>
        </div>
        <ul class="log-out">
          <li> <a href="sign-out.php">Log Out</a> </li>
        </ul>
      </div>
    </nav>
    <div class="main-body">
      <div class="get-app">
        <img src="images/get-app.png" alt="get-app">
        <h3>Opportunity For Everyone</h3>
        <p>You can apply to any company, anytime, get information about any company
            Most important you can get information about your application.</p>
        <div class="mobile-app">
          <a href="#">GET MOBILE APP</a>
        </div>
        <div class="mobile">
           <img src="images/mobile.png" alt="mobile">
        </div>
      </div>
      <div class="main-user-dash">
        <div class="user-services">
          <div class="user-profile">
            <?php echo "<img src='user_images/".$_SESSION['profile']."' >";?>
          </div>
          <div class="service-part">
            <ul>
              <li class="active-service"><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Account overview</a></li>
              <li ><a href="dashboard.php"><i class="fa fa-home" aria-hidden="true"></i>Dashboard</a></li>
              <li><a href="#"><i class="fa fa-paint-brush" aria-hidden="true"></i>Edit profile</a></li>
              <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i>Your applications</a></li>
              <li><a href="available-companies.php"><i class="fa fa-eye" aria-hidden="true"></i>Available companies</a></li>
              <li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a></li>
              <li><a href="app_store.php"><i class="fa fa-puzzle-piece" aria-hidden="true"></i>Apps</a></li>
              <li><a href="#"><i class="fa fa-history" aria-hidden="true"></i>History</a></li>
              <li><a href="#"><i class="fa fa-trash" aria-hidden="true"></i>Deleted Data</a></li>
              <li><a href="sign-out.php"><i class="fa fa-sign-out-alt" aria-hidden="true"></i>Log Out</a></li>
            </ul>
          </div>
        </div>
        <div class="about-service">
            <div class="account-info">
              <h2>Account overview</h2>
              <h3>Profile</h3>
              <table>
                <tbody>
                  <tr>
                    <tr>
                  </tr>
                    <tr>
                      <td>User Name</td>
                      <td> <?php echo $_SESSION['name'] ; ?> </td>
                    </tr>
                  </tr>
                    <tr>
                      <td>Email</td>
                      <td> <?php echo $_SESSION['email'] ; ?> </td>
                    </tr>
                  </tr>
                </tr>
                  <tr>
                    <td>gender</td>
                    <td> <?php echo $_SESSION['gender']; ?> </td>
                  </tr>
                </tr>
                </tbody>
              </table>
            </div>
            <a class="edit-profile" href="edit_profile.php">Edit profile</a>
            <div class="container">
              <div class="row">
                <div class="statistic col-md-8">
                  <!-- <img src="images/statistic.png" class="rounded" alt="statistic"> -->
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
  </body>
</html>
