<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Opt Lab | Sign Up</title>
    <link rel="stylesheet" href="styles/signup.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="m-auto col-xs-6 col-md-5 col-sm-12">
                <div class="signup-form">
						<form action="signup_process.php" method="post"  enctype="multipart/form-data">
							<h1>Register Now</h1>
							<div class="input-group">
								<input type="text" name="name" placeholder="Full Name" class="input" required>
									</div>
										<div class="form-wrapper">
											<input type="email" name="email" placeholder="Email Address" class="input" required>
											</div>
											<div class="form-wrapper">
												<select name="gender" id="" class="input" required>
													<option value="" disabled selected>Gender</option>
													<option value="male">Male</option>
													<option value="femal">Female</option>
													<option value="other">Other</option>
												</select>
											</div>
											<div class="form-controler">
												<input type="password" name="password" placeholder="Password" class="input" required>
											</div>
											<div class="form-controler">
												<label>Upload your profile:</label>
												<input type="file" name="img" required/>
											</div>
                      <div class="private-signin">
                      <h3>By Registering You Agree With <a href="private_policy.php">Private Policy</a> </h3>
                        <h4>Alread Have Account? <a href="signin.php">Sign In</a></h4>
                      </div>
													<input type="submit" name="register-user" value="Register">
												</form>
											</div>

            </div>
        </div>
    </div>
</body>
<link rel="stylesheet" href="../Bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="../Bootstrap/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/e5644a9822.js" crossorigin="anonymous"></script>
</html>