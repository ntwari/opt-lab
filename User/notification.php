<?php
session_start();
?>
<?php 
    include("../includes/dbconfig.php");
?>
<?php
//   if($_SESSION['name']=='')
//   {
//      header('location:signin.php');
//   }

 ?>
<?php
function timeAgo($time_ago){

			$time_ago = strtotime($time_ago);
			$cur_time   = time();
			$time_elapsed   = $cur_time - $time_ago;
			$seconds    = $time_elapsed ;
			$minutes    = round($time_elapsed / 60 );
			$hours      = round($time_elapsed / 3600);
			$days       = round($time_elapsed / 86400 );
			$weeks      = round($time_elapsed / 604800);
			$months     = round($time_elapsed / 2600640 );
			$years      = round($time_elapsed / 31207680 );
			// Seconds
			if($seconds <= 60){
			    return "just now";
			}
			//Minutes
			else if($minutes <=60){
			    if($minutes==1){
			        return "one minute ago";
			    }
			    else{
			        return "$minutes minutes ago";
			    }
			}
			//Hours
			else if($hours <=24){
			    if($hours==1){
			        return "an hour ago";
			    }else{
			        return "$hours hrs ago";
			    }
			}
			//Days
			else if($days <= 7){
			    if($days==1){
			        return "yesterday";
			    }else{
			        return "$days days ago";
			    }
			}
			//Weeks
			else if($weeks <= 4.3){
			    if($weeks==1){
			        return "a week ago";
			    }else{
			        return "$weeks weeks ago";
			    }
			}
			//Months
			else if($months <=12){
			    if($months==1){
			        return "a month ago";
			    }else{
			        return "$months months ago";
			    }
			}
			//Years
			else{
			    if($years==1){
			        return "one year ago";
			    }else{
			        return "$years years ago";
			    }
			}
		} 
?>

<?php 
$sql="SELECT * FROM `registration` WHERE `name`='".$_SESSION['name']."'";
	$result = $con->query($sql);
			if ($result->num_rows>0)
				 {    
			    while($row = $result->fetch_assoc()) 
			    {	    
		$image=$row['image'];
		$applicant=$row["usr_id"];
}
}
 ?>


<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/notification.css">
  </head>
  <body>
    <nav class="nav">
        <div class="logo">
          <a href="../index.php">
            <p><span>OPT</span>Lab</p>
          </a>
        </div>
      <div class="menu">
        <ul class="menu-pages">
          <li> <a href="home.php" class="active">Home</a> </li>
          <li> <a href="#">About Us</a> </li>
          <li> <a href="#">Help</a> </li>
        </ul>
        <div class="header_profile">
          <?php echo "<img src='user_images/".$_SESSION['profile']."' >";?>
        </div>
        <ul class="log-out">
          <li> <a href="sign-out.php">Log Out</a> </li>
        </ul>
      </div>
    </nav>
    <div class="main-body">
      <div class="main-user-dash">
        <div class="user-services">
          <div class="user-profile">
            <?php echo "<img src='user_images/".$_SESSION['profile']."' >";?>
          </div>
          <div class="service-part">
            <ul>
              <li><a href="home.php"><i class="fa fa-user" aria-hidden="true"></i>Account overview</a></li>
              <li><a href=""><i class="fa fa-home" aria-hidden="true"></i>Dashboard</a></li>
              <li><a href="#"><i class="fa fa-paint-brush" aria-hidden="true"></i>Edit profile</a></li>
              <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i>Your applications</a></li>
              <li><a href="available-companies.php"><i class="fa fa-eye" aria-hidden="true"></i>Available companies</a></li>
              <li class="active-service"><a href="#"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a></li>
              <li><a href="app_store.php"><i class="fa fa-puzzle-piece" aria-hidden="true"></i>Apps</a></li>
              <li><a href="#"><i class="fa fa-history" aria-hidden="true"></i>History</a></li>
              <li><a href="#"><i class="fa fa-trash" aria-hidden="true"></i>Deleted Data</a></li>
              <li><a href="sign-out.php"><i class="fa fa-sign-out-alt" aria-hidden="true"></i>Log Out</a></li>
            </ul>
          </div>
        </div>
        <div class="about-service">
        <div class="col-md-10 notification-table">
  <div class="table-responsive" style="margin-top: 2em;">
  <table class="table table-bordered table-hover table-striped">
  <thead class = "thead-dark">
  <tr class="active">
    <th style="text-align:center">#</th>
    <th style="text-align:center">Sender</th>
    <th style="text-align:center">Comment</th>
    <th style="text-align:center">Date</th>
    <th style="text-align:center">Reply</th>
    <th style="text-align:center">Delete</th>
  </tr>
</thead>
<?php 


    $uid=$_SESSION["id"];
    
    $sql1="select * from notification where post_usr='$uid' order by noti_id DESC ";
    $re=mysqli_query($con,$sql1);
    $count=mysqli_num_rows($re);
    $i=1;
    while($row=mysqli_fetch_array($re))
    {
        $from_app=$row["from_appl"];
        $no_id=$row["noti_id"];
        $po_id=$row["pos_id"];
        $post_user_id=$row["post_usr"];
        $comment_user_id=$row["comm_user"];
        $co=$row["comment"];
        $ti=$row["time"];
        $post_id=$_SESSION["id"];
        echo '<tr class="default">';
           echo '<td style="text-align:center">'.$i.'</td>';
               
    $user=mysqli_query($con,"select * from registration where usr_id='".$comment_user_id."'");
    $userRes=mysqli_fetch_array($user);
    
    if(!$from_app){
        echo '<td>'.$userRes['name'].'</td>';
           echo '<td>'.$co.'</td>';
           echo '<td>'.$ti.'</td>';
           echo '<td><form method="post" action="reply.php">
           <input type="hidden" name="comment" value="'.$co.'" />
           <input type="hidden" name="po_id" value="'.$post_id.'" />
           <input type="hidden" name="commenter_id" value="'.$comment_user_id.'" />
           <input type="submit" name="reply" class="btn btn-success" value="Reply" id="reply"/>
           </form></td>';
           echo '<td><a href="javascript:delete_student('.$no_id.')" class="btn btn-danger" id="delete">delete</a></td>';
        echo '</tr>'; 
        $i++;  
    }
    else{
        $sql2="SELECT *  FROM company WHERE comp_id=".$userRes["usr_id"]." ";
        $query2=mysqli_query($con,$sql2);
        $company=mysqli_fetch_array($query2);
    echo "<div class='from_admin'>";
      echo '<td class="from_admin">From '.$company["comp_name"].'</td>';
      echo '<td>'.$co.'</td>';
      echo '<td>'.$ti.'</td>';
      echo '<td><form method="post" action="reply.php">
      <input type="hidden" name="comment" value="'.$co.'" />
      <input type="hidden" name="po_id" value="'.$post_id.'" />
      <input type="hidden" name="commenter_id" value="'.$comment_user_id.'" />
      <input type="submit" name="reply" class="btn btn-success" value="Reply" id="reply"/>
      </form></td>';
      echo '<td><a href="javascript:delete_student('.$no_id.')" class="btn btn-danger" id="delete">delete</a></td>';
      echo "<div>";
   echo '</tr>'; 
   $i++;  
    }
  }
?>
</table>
</div>
        </div>
      </div>
    </div>
    <script>
  function delete_student(id)
  {
    if(confirm("You want to delete comment ?"))
    {
    window.location.href='../user-1/execution.php?yesdelete='+id;
    }
  }
</script>
  </body>
</html>
