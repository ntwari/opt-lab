<?php
session_start();
include "../includes/dbconfig.php";
?>
<?php
function timeAgo($time_ago){

			$time_ago = strtotime($time_ago);
			$cur_time   = time();
			$time_elapsed   = $cur_time - $time_ago;
			$seconds    = $time_elapsed ;
			$minutes    = round($time_elapsed / 60 );
			$hours      = round($time_elapsed / 3600);
			$days       = round($time_elapsed / 86400 );
			$weeks      = round($time_elapsed / 604800);
			$months     = round($time_elapsed / 2600640 );
			$years      = round($time_elapsed / 31207680 );
			// Seconds
			if($seconds <= 60){
			    return "just now";
			}
			//Minutes
			else if($minutes <=60){
			    if($minutes==1){
			        return "one minute ago";
			    }
			    else{
			        return "$minutes minutes ago";
			    }
			}
			//Hours
			else if($hours <=24){
			    if($hours==1){
			        return "an hour ago";
			    }else{
			        return "$hours hrs ago";
			    }
			}
			//Days
			else if($days <= 7){
			    if($days==1){
			        return "yesterday";
			    }else{
			        return "$days days ago";
			    }
			}
			//Weeks
			else if($weeks <= 4.3){
			    if($weeks==1){
			        return "a week ago";
			    }else{
			        return "$weeks weeks ago";
			    }
			}
			//Months
			else if($months <=12){
			    if($months==1){
			        return "a month ago";
			    }else{
			        return "$months months ago";
			    }
			}
			//Years
			else{
			    if($years==1){
			        return "one year ago";
			    }else{
			        return "$years years ago";
			    }
			}
		} 
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Home</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <link rel="stylesheet" href="styles/available-companies.css">
  </head>
  <body>
    <nav class="nav">
        <div class="logo">
          <a href="../index.php">
            <p><span>OPT</span>Lab</p>
          </a>
        </div>
      <div class="menu">
        <ul class="menu-pages">
          <li> <a href="home.php" class="active">Home</a> </li>
          <li> <a href="#">About Us</a> </li>
          <li> <a href="#">Help</a> </li>
        </ul>
        <div class="header_profile">
          <?php echo "<img src='user_uploads/".$_SESSION['profile']."' >";?>
        </div>
        <ul class="log-out">
          <li> <a href="sign-out.php">Log Out</a> </li>
        </ul>
      </div>
    </nav>
      <div class="main-user-dash">
        <div class="user-services">
          <div class="user-profile">
            <?php echo "<img src='user_uploads/".$_SESSION['profile']."' >";?>
          </div>
          <div class="service-part">
            <ul>
              <li><a href="#"><i class="fa fa-user" aria-hidden="true"></i>Account overview</a></li>
              <li ><a href=""><i class="fa fa-home" aria-hidden="true"></i>Dashboard</a></li>
              <li><a href="#"><i class="fa fa-paint-brush" aria-hidden="true"></i>Edit profile</a></li>
              <li><a href="#"><i class="fa fa-comments" aria-hidden="true"></i>Your applications</a></li>
              <li class="active-service"><a href="available-companies.php"><i class="fa fa-eye" aria-hidden="true"></i>Available companies</a></li>
              <li><a href="#"><i class="fa fa-bell" aria-hidden="true"></i>Notifications</a></li>
              <li><a href="app_store.php"><i class="fa fa-puzzle-piece" aria-hidden="true"></i>Apps</a></li>
              <li><a href="#"><i class="fa fa-history" aria-hidden="true"></i>History</a></li>
              <li><a href="#"><i class="fa fa-trash" aria-hidden="true"></i>Deleted Data</a></li>
              <li><a href="sign-out.php"><i class="fa fa-sign-out-alt" aria-hidden="true"></i>Log Out</a></li>
            </ul>
          </div>
        </div>
        <div class="about-service">
        <div class="col-md-6" style="margin-left: 28em;margin-top:2em;">
						<?php 
						$num_rec_per_page=10;
						if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
                          $start_from = ($page-1) * $num_rec_per_page; 
                            $sql=mysqli_query($con,"select * from post join company where post.comp_id=company.comp_id order by post.posted_date desc limit  $start_from, $num_rec_per_page"); 
                            // $sql=mysqli_query($con,"select * from post inner join company where post.comp_id=company.comp_id order by post.posted_date");
                             while($row=mysqli_fetch_array($sql))
                               {
								$proj_name=$row["career_name"];
								$project=$row["post_id"];
     	                           $id=$row["post_id"];
     	                           $like=$row["like"];
     	                           $dislike=$row["unlike"];
     	                           $time=$row["posted_date"];
                                    // <div class="post-show" style="width:90%;border-radius:5px">
						echo '<div class="post-show col-md-4">
									<div class="post-show-inner">
										<div class="post-header">
											<div class="post-left-box">
												<div class="id-img-box"><a href="otheruser.php?userid='.$row['post_id'].'">
                                                 </div>
													<ul>
													<b><a href="otheruser.php?userid='.$row['comp_id'].'" class="name">	'.$row['comp_name'].'</a> </b>
														<li class="time-counter">'.timeAgo($time).'</li>
													</ul>
												</div>
											</div>
											<div class="post-right-box"></div>
										</div>
									
											<div class="post-body">
							<div class="post-header-text">
							 <a href="project_description.php?id='.$id.'&s_title=' . $row['career_desc'] . '" class="post-title">'.$row['career_name'].'</a>
							  
							                 <p class="status">'.$row['career_desc'].'</p>

											';
										$sql1=mysqli_query($con,"select * from comments where post_id_c='$id'");
							
										while($row1=mysqli_fetch_array($sql1))
										{	 
                                            $ct=$row1["comment_time"];
                                            $c=$row1["comment"];
                                            $uid=$row1["user_id_c"];
                                            $sql2=mysqli_query($con,"select * from registration where usr_id='$uid'");
                                            while($row2=mysqli_fetch_array($sql2))
										     {
                                                 $n=$row2["name"];
												 $img=$row2["image"];
                                                 echo '<div style="margin-left:50px;">
										<a href="otheruser.php?userid='.$uid.'"><img style="height:40px; width="40px" src="user_images/'.$img.'"></img></a>
										    <p class="comments">'.$c.'</p>
											 </div>
											 <div style="margin-left:50px"><div class="id-name">
													<ul style="color: #7f8c8d;font-size: 1em;font-weight: 100;">
														<small>'.timeAgo($ct).'</small> &nbsp; &nbsp; &nbsp;<font style="color:#2c3e50;font-family: roboto "> comment by :</font> <font style="font-size:12px"><a href="otheruser.php?userid='.$uid.'" class="comment-by"> '.$n.'</a></font>
													</ul>
											</div>
										</div>
										
										';
										     }
									   }
							echo '<a href="project_description.php?id='.$id.'&s_title=' . $row['career_desc'] . '" class="myButton">Post Your Comment</a>
                        
                                <div class="aplly">
                                    <a href="application.php?applicant='.$applicant.'&company='.$row["comp_id"].'&post='.$row["post_id"].'" class="apply"> Apply now
                                </div>
							</div>
        
								</div>
								</div></div><br/> ';	
     	                           
                            }                    
?>                   
</div>
        </div>
    </div>
  </body>
</html>