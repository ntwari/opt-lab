<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OptLab | Log in</title>
    <link rel="stylesheet" href="styles/signin.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="m-auto col-md-5 col-xs-6 col-sm-12">
                <div class="signin_form">
                    <h2>Log in to Your OPTLab account</h2>
                    <form action="signin-check.php" method="post">
                        <input  type="email" name="user_email" class="input" placeholder="example@gmail.com" required>
                        <input  type="password" name="user_password" class="input" placeholder="Enter Your Password" required>
                        <input type="submit" name="sign_in" value="Login">
            <div class="forget-signup">
                <a href="forget_password.php" class="forget-password">Forget Password</a>
                <h4>Not Having Account? <a href="signup.php">Sign Up</a></h4>
            </div>

        </form>
                </div>
            </div>
        </div>
    </div>
</body>
<link href="https://fonts.googleapis.com/css?family=Fjalla+One|PT+Sans+Narrow&display=swap" rel="stylesheet">
<link rel="stylesheet" href="../Bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="../Bootstrap/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/e5644a9822.js" crossorigin="anonymous"></script>
</html>