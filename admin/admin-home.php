<?php
   session_start();
   if($_SESSION['email']=='')
   {
      header('location:login.php');
   }
   include("../includes/dbconfig_admin.php");
?>
<?php
    $sql="SELECT * FROM admins WHERE email=:email";
    $query=$connect->prepare($sql);
    $query->bindParam(":email",$_SESSION["email"]);
    $query->execute();
    while ($row=$query->fetch()) {
        $admin_id=$row["admin_id"];
        $comp_id=$row["comp_id"];
        $name=$row["admin_first_name"]." ".$row["admin_last_name"];
        $email=$row["email"];
        $profile=$row["profile"];
        $national_id=$row["national_id"];
        $phone=$row["phone"];
        if(!$profile){
            $profile="user-1.jpg";
          }
        $sql1="SELECT * FROM company WHERE comp_id=:comp_id";
        $query1=$connect->prepare($sql1);
        $query1->bindParam(":comp_id",$comp_id);
        $query1->execute();
        while ($rows=$query->fetch()) {
            $comp_name=$rows["comp_name"];
        }
    }
?>
<?php
 if(isset($_POST["update"])){
    $full_name=$_POST["full_name"];
    $email=$_POST["email"];
    $phone=$_POST["phone"];
    $national_id=$_POST["national"];

    $sql3="UPDATE admins SET admin_first_name=:full_name,email=:email,phone=:phone,national_id=:national WHERE admin_id=:admin_id";
    $query3=$connect->prepare($sql3);
    $query3->bindParam(":full_name",$full_name);
    $query3->bindParam(":email",$email);
    $query3->bindParam(":phone",$phone);
    $query3->bindParam(":national",$national_id);
    $query3->bindParam(":admin_id",$admin_id);
    if($query3->execute()){
        echo "
         <script>
         alert('Hello $full_name You have updated your profile now');
         </script>
         ";
    }
    else{
        echo "
         <script>
         alert(Ooops Failed yo update your profile');
         </script>
         ";
    }
 }
 if(isset($_POST["update_profile"])){
$pic=$_FILES["img"]["name"];
$tmp=$_FILES["img"]["tmp_name"];
$type=$_FILES["img"]["type"];
$path="images/".$pic;

move_uploaded_file($tmp,$path);
$sql4="UPDATE admins SET profile=:profile WHERE admin_id=:admin_id";
$query4=$connect->prepare($sql4);
$query4->bindParam(":profile",$pic);
$query4->bindParam(":admin_id",$admin_id);
if($query4->execute()){
    echo "
    <script>
    alert(Yeap!! you have updated profile picture');
    location.reload();
    </script>
    ";
    header("location:admin-home.php");
}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles/home.css">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <title>Admin Home</title>
</head>
<body>
    <div class="nav">
        <div class="logo">
            <a href="">
                <p><span>OPT</span>Lab</p>
            </a>
        </div>
        <div class="nav-buttons">
            <ul>
                <li class="active-nav-btn"><a href="home.php">Home</a></li>
                <li><a href="#">Manage-Account</a></li>
                <li><a href="signout.php">Log Out</a></li>
            </ul>
        </div>
    </div>
    <div class="mainbody">
        <div class="functionalities">
            <div class="admin-functionalities">
            <?php 
                 echo ' <img src="images/'.$profile.'" width="300px" height="300px">';
                        ?><br>
            </div>
                <div class="functionality-menu">
                    <ul>
                        <li class="active-service"> <a href="admin-home.php">Dash Board</a> </li>
                        <li> <a href="comp.php">Company Overview</a> </li>
                        <li> <a href="applicants.php">All Applicants</a> </li>
                        <li> <a href="voted_applicants.php">Voted Applicants</a> </li>
                        <li> <a href="interview-room.php">Interview room</a> </li>
                        <li> <a href="notification.php">Notifications</a> </li>
                        <li> <a href="admin-login.php">Log out</a> </li>
                    </ul>
            </div>
        </div>
        <div class="functionality-desc">
            <div class="container">
                <div class="profile">
                    <div class="profile_pic">
                        <?php 
                            echo ' <img src="images/'.$profile.'" width="200px" height="200px">';
                        ?><br>
                        <form action="#" method="post" style="margin-left: 3em;"  enctype="multipart/form-data">
                        <input type="file" name="img" style="margin-left: 1em;"><br>
                        <input type="submit" value="update pic" class="profile" name="update_profile">
                        </form>
                    </div>
                    <div class="other_profiles">
                        <form action="#" method="post">
                            <label>Full name:</label><br>
                            <input type="text" name="full_name" value="<?php echo $name?>"><br><br>
                            <label>Email:</label><br>
                            <input type="email" name="email" value="<?php echo $email?>"><br><br>
                            <label>Get in touch with us on:</label><br>
                            <input type="number" name="phone" value="<?php echo $phone?>"><br><br>
                            <label>NATIONAL ID:</label><br>
                            <input type="number" name="national" value="<?php echo $national_id?>" contentable="false"><br><br>
                            <input type="submit" name="update" class="update" value="update">
                        </form>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</body>
</html>