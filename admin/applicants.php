<?php
   session_start();
   if($_SESSION['email']=='')
   {
      header('location:login.php');
   }
   include("../includes/dbconfig_admin.php");
?>
<?php
    $sql="SELECT * FROM admins WHERE email=:email";
    $query=$connect->prepare($sql);
    $query->bindParam(":email",$_SESSION["email"]);
    $query->execute();
    while ($row=$query->fetch()) {
        $comp_id=$row["comp_id"];
    }
?>
<?php
    $sql="SELECT * FROM admins WHERE email=:email";
    $query=$connect->prepare($sql);
    $query->bindParam(":email",$_SESSION["email"]);
    $query->execute();
    while ($row=$query->fetch()) {
        $admin_id=$row["admin_id"];
        $comp_id=$row["comp_id"];
        $name=$row["admin_first_name"]." ".$row["admin_last_name"];
        $email=$row["email"];
        $profile=$row["profile"];
        $national_id=$row["national_id"];
        $phone=$row["phone"];
        if(!$profile){
            $profile="user-1.jpg";
          }
        $sql1="SELECT * FROM company WHERE comp_id=:comp_id";
        $query1=$connect->prepare($sql1);
        $query1->bindParam(":comp_id",$comp_id);
        $query1->execute();
        while ($rows=$query->fetch()) {
            $comp_name=$rows["comp_name"];
        }
    }
?>
<?php
    if(isset($_POST["allow"])){
        $applicant=$_POST["applicant_id"];
        $allowed_status=2;
        $sql="UPDATE application SET status=:allowed_status WHERE id=:appl_id";
        $query=$connect->prepare($sql);
        $query->bindParam(":allowed_status",$allowed_status);
        $query->bindParam(":appl_id",$applicant);
        if($query->execute()){
      echo "
                <script>
                  confirm('Applicant now is allowed to join the interview, then you can notify him about the intervew date')
    </script>
                ";
        }
        else{
      echo "
                <script>
                  alert('data notttttttttttttw submited');
    </script>
                ";
        }

    //   echo "
    //             <script>
    //               alert('data now submited');
    // </script>
    //             ";
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles/applicants.css">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <title>Admin | Applicants</title>
    <link href="https://fonts.googleapis.com/css?family=Saira+Condensed&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
    <div class="nav">
        <div class="logo">
            <a href="">
                <p><span>OPT</span>Lab</p>
            </a>
        </div>
        <div class="nav-buttons">
            <ul>
                <li><a href="admin-home.php">Home</a></li>
                <li><a href="#">Manage-Account</a></li>
                <li><a href="signout.php">Log Out</a></li>
            </ul>
        </div>
    </div>
    <div class="mainbody">
        <div class="functionalities">
            <div class="admin-functionalities">
            <?php 
                 echo ' <img src="images/'.$profile.'" width="300px" height="300px">';
                        ?><br>
            </div>
                <div class="functionality-menu">
                <ul>
                        <li> <a href="admin-home.php">Dash Board</a> </li>
                        <li> <a href="comp.php">Company Overview</a> </li>
                        <li class="active-service"> <a href="applicants.php">All Applicants</a> </li>
                        <li> <a href="voted_applicants.php">Voted Applicants</a> </li>
                        <li> <a href="interview-room.php">Interview room</a> </li>
                        <li> <a href="notification.php">Notifications</a> </li>
                        <li> <a href="admin-login.php">Log out</a> </li>
                    </ul>
            </div>
        </div>
        <div class="functionality-desc">
        <div class = "container applicants">
          <table class = "table table-bordered table-striped table-hover">
            <thead class = "thead-dark">
                 <tr>
                     <th>#</th>
                     <th>Full Names</th>
                      <!-- <th>User Name</th> -->
                     <th>Email</th>
                      <th>phone</th>
                      <th>CVS</th>
                      <!-- <th>Notify</th> -->
                      <th colspan="2" class="text-center">Actions</th>
                      <!-- <th>Interview</th> -->
                      <!-- <th>Winner</th> -->
                 </tr>
            </thead>
            <tbody>
            <?php
                  include("../includes/dbconfig_admin.php");
                          # <div class = 'text-center' id = 'status'></div>
              $sql="SELECT * FROM application WHERE manager=:company";
              $query=$connect->prepare($sql);
              $query->bindParam(":company",$comp_id);
              $query->execute();
              $number_available=1;
              $number_of_rows = $query->rowCount();
             echo "<p class='applicants'>The number of applicants :".$number_of_rows."</p>";
              while ($row=$query->fetch()) {
                echo "
                <tr>
                   <td>".$number_available."</td>
                   <td>".$row["full_name"]."</td>
                   <td>".$row["email"]."</td>
                   <td>".$row["phone"]."</td>
                   <td class='open_cv'><a class='btn btn-info' href='../cvs/".$row["cv"]."' target='_blank'>Open</a></td>
            ";
            // <td id="interview">interview</td>
            //  <td id="notify" class="notify"><button class="btn btn-warning">notify</button></td>
            //  <td class="winner"><button class="btn btn-success">Allow</button></td>
            //<td class="winner"><button class="btn btn-success" name="allow">Allow</button></td>
             echo '
                <form action="#" method="post">
                   <input type="hidden" name="applicant_id" value='.$row["id"].'>
                    <td class="winner"><input type="submit" class="btn btn-success" name="allow" value="allow"</td>
                </form>
                    <td id="notify" class="notify"><button onclick="notify()" class="btn btn-danger">notify</button></td>
                    </tr>
             ';

            echo '
            <div id="contact">
            <form action="../user-1/comment1.php" method="post">
              <textarea required name="comment" placeholder="Notify '.$row["full_name"].'" cols="30" rows="3" class="textarea" style="border: 1px solid #2980b9;border-radius: 1em;outline: none;"></textarea>
              <input class="form-control"  type="hidden" name="uid" value="'.$comp_id.'">
              <input type="hidden" name="from_admin" value="1">
              <input class="form-control"  type="hidden" name="pid" value="'.$row["task_id"].'">
              <input class="form-control"  type="hidden" name="comme_id" value="'.$row["applicant"].'>"><br>
              <div class="btn-group" role="group" style="margin-top:5px"><br>
                <button type="submit" name="submit_comment"  class="btn btn-primary btn-hover-green">Notify Him</button>
                  </div>
            </form><br><br>
          </div>
            ';
            $number_available++;      
              }
                    // <tfoot>
                    //        <tr>
                    //             <td colspan = '6'>
                    //                 <p class = 'text-center'>DEAR ".$_SESSION["FIRSTNAME"]." YOU ARE AN ADMIN</p>
                    //             </td>
                    //         </tr>
                    //     <tfoot>   
              
            ?>
            </tbody>
        </div>
    </div>
    <script> 
$(document).ready(function(){
  $("#notify").click(function(){
    $("#contact").slideToggle(1400);
  });
//   $("#notify").click(function(){
//     $("#contact").slideUp(1400);
//     $("#contact").style.display="none";
//   });
});
// function notify(){
//     document.getElementById("contact").style.display="block";
// }
</script>
</body>
</html>
