<?php 
$admin=$_GET["admin_id"];
$applicant_id=$_GET["appl_id"];

include("../includes/dbconfig_admin.php");
// $query="SELECT * FROM registration WHERE usr_id=".$admin." ";
$sql="SELECT * FROM registration WHERE usr_id=:applicant";
$query=$connect->prepare($sql);
$query->bindParam(":applicant",$applicant_id);
$query->execute();
while($row=$query->fetch()){
  $applicant_images=$row["image"];
  $applicant_name=$row["name"];
    // $query1="SELECT * FROM registration WHERE usr_id=".$applicant_id."";
    $sql1="SELECT * FROM admins WHERE comp_id=:admin_id";
    $query1=$connect->prepare($sql1);
    $query1->bindParam(":admin_id",$admin);
    $query1->execute();
    while($row1=$query1->fetch()){
      $admin_image=$row1["profile"];
      $admin_name=$row1["admin_first_name"]." ".$row1["admin_last_name"];
      if(!$admin_image){
        $admin_image="user-1.jpg";
      }
    }
}
?>
<?php 
$sql="SELECT * FROM application WHERE applicant=:applicant";
$query=$connect->prepare($sql);
$query->bindParam(":applicant",$applicant_id);
if($query->execute()){
    while ($row=$query->fetch()) {
        $interview=$row["interview"];
        // echo "<script> alert('$interview') </script>";
     }
}
?>
<!DOCTYPE HTML>
<html>
<head> 
        <meta charset=utf-8>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script language="JavaScript" type="text/javascript" src="//comet-server.com/js/lib/jquery.min.js" ></script>
        <script src="//comet-server.com/CometServerApi.js" type="text/javascript"></script>
        <script src="//comet-server.com/cometVideoApi.js" type="text/javascript"></script> 
  <link rel="stylesheet" type="text/css" href="../css1/manager.css">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Tangerine">
          <link href="https://fonts.googleapis.com/css?family=Allerta+Stencil|Black+Ops+One|Fredericka+the+Great|Nanum+Pen+Script|Oleo+Script|Satisfy&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Palanquin&display=swap" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Khand|Palanquin|Viga|Yantramanav&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed&display=swap" rel="stylesheet">
</head>
<style>
body{
    background: #ecf0f1;
}
.manage_title h1{font-family:'Khand', sans-serif;margin-left: 13em}
.user_id{margin-left: 33em}
button:hover{background: #d35400;transition: ease-in-out 0.4s;cursor: pointer;}
#mute{border:none;border-radius: 5px;height: 3em;width: 3em;}
#mute i{font-size: 2em;outline: none}
#mute:hover{color: #fff;cursor: pointer;}
.open_book{float: left;}
.open_book button{outline: none;}
.open_book button:hover{background: #27ae60;}
.sidenav_index {
	height: 100%;width: 60%;position: fixed;z-index: 1;top: 0;right: 0;background: #fff;overflow-y: hidden;transition: 0.5s;padding-top: 60px;text-align:center;border-left: 10px solid #27ae60;border-right: 10px solid #27ae60;margin-right: 15em;margin-top: 1em;}
  
  .sidenav_index a {
	padding: 8px 8px 8px 32px;text-decoration: none;font-size: 25px;color: #818181;display: block;transition: 0.3s; }
  
  .sidenav_index a:hover{
	color: #f1f1f1; }
  
  .sidenav_index .closebtn {
	position: absolute;top: 0;right: 25px;font-size: 36px;margin-left: 50px;
  }
  
@media screen and (max-height: 450px) {
.sidenav_index {padding-top: 15px;}
.sidenav_index a {font-size: 18px;}
  }
.closebtn:hover{background: linear-gradient( to right,#2ecc71,#2ecc71,#2ecc71);border: none;border-radius: 50%;}
.design{background: linear-gradient(-90deg,#2ecc71,#93f1ba,#3cfa8b););height: 80%;margin: 1em;-webkit-box-shadow: inset 14px 6px 23px -2px rgba(0,0,0,0.61);-moz-box-shadow: inset 14px 6px 23px -2px rgba(0,0,0,0.61);box-shadow: inset 14px 6px 23px 2px rgba(0,0,0,0.61);}
.resume{background: linear-gradient(90deg,white,#bdc3c7);height: 50%;box-shadow: -113px -85px 78px -60px rgba(0,0,0,0.5);}
.resume h1{font-family:'Khand', sans-serif;color: #34495e;text-transform: uppercase;}
.design .interview{border: 1px solid #34495e;outline: none;border-radius: 10px;margin-top: 3px;font-family: 'Barlow Condensed', sans-serif;font-size: 1.3em;text-align: left:;margin: 3em;height: 8em;background-color: #fff;}
.submit_button{	background-color: #34495e;border-radius:28px;border:1px solid #18ab29;display:inline-block;cursor:pointer;color:#ffffff;font-family:Arial;font-size:17px;padding:16px 76px;text-decoration:none;text-shadow:0px 1px 0px #2f6627;}
.submit_button:hover {
	background-color: #7f8c8d;transition: ease-in-out 0.4s;
}
.myButton:active {
	position:relative;top:1px;
}
.headers{display: flex;flex-wrap: wrap;background: #34495e;color: #fff;margin: -1em;padding: 1em;border-bottom: 1em solid #27ae60;}
.desc{font-family:'Khand', sans-serif;color: #fff;}
</style>
<body>
<div id="mySidenav" class="sidenav_index">
  <div class="design">
     <div class="resume">
        <h1>BOOKLET FOR <?php echo $applicant_name?></h1>
        <?php echo '<img style="width: 130px;height: 130px;border: none;border-radius: 50%;" src="../user/user_images/'.$applicant_images.'"';?>
     </div>
     <h1>READ ABOUT THE INTERVIEW</h1>
  </div>
  <div>
     <div class="interview">
        <?php echo "<p>".$interview."</p>"?>
     </div>
  </div>
</div>
</body>
</html>