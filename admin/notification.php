<?php
   session_start();
   if($_SESSION['email']=='')
   {
      header('location:login.php');
   }
   include("../includes/dbconfig_admin.php");
?>
<?php
    $sql="SELECT * FROM admins WHERE email=:email";
    $query=$connect->prepare($sql);
    $query->bindParam(":email",$_SESSION["email"]);
    $query->execute();
    while ($row=$query->fetch()) {
        $comp_id=$row["comp_id"];
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles/applicants.css">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <title>Admin | Applicants</title>
</head>
<body>
    <div class="nav">
        <div class="logo">
            <a href="">
                <p><span>OPT</span>Lab</p>
            </a>
        </div>
        <div class="nav-buttons">
            <ul>
                <li><a href="admin-home.php">Home</a></li>
                <li><a href="#">Manage-Account</a></li>
                <li><a href="signout.php">Log Out</a></li>
            </ul>
        </div>
    </div>
    <?php
    $sql="SELECT * FROM admins WHERE email=:email";
    $query=$connect->prepare($sql);
    $query->bindParam(":email",$_SESSION["email"]);
    $query->execute();
    while ($row=$query->fetch()) {
        $admin_id=$row["admin_id"];
        $comp_id=$row["comp_id"];
        $name=$row["admin_first_name"]." ".$row["admin_last_name"];
        $email=$row["email"];
        $profile=$row["profile"];
        $national_id=$row["national_id"];
        $phone=$row["phone"];
        if(!$profile){
            $profile="user-1.jpg";
          }
        $sql1="SELECT * FROM company WHERE comp_id=:comp_id";
        $query1=$connect->prepare($sql1);
        $query1->bindParam(":comp_id",$comp_id);
        $query1->execute();
        while ($rows=$query->fetch()) {
            $comp_name=$rows["comp_name"];
        }
    }
?>
    <div class="mainbody">
        <div class="functionalities">
            <div class="admin-functionalities">
            <?php 
                 echo ' <img src="images/'.$profile.'" width="300px" height="300px">';
                        ?><br>
            </div>
                <div class="functionality-menu">
                <ul>
                <li> <li><a href="admin-home.php">Dash Board</a> </li>
                        <li> <a href="comp.php">Company Overview</a> </li>
                        <li> <a href="applicants.php">All Applicants</a> </li>
                        <li> <a href="voted_applicants.php">Voted Applicants</a> </li>
                        <li> <a href="interview-room.php">Interview room</a> </li>
                        <li class="active-service"> <a href="notification.php">Notifications</a> </li>
                        <li> <a href="admin-login.php">Log out</a> </li>
                    </ul>
            </div>
        </div>
        <div class="functionality-desc">
        <div class = 'container applicants'>
        <table class="table table-bordered table-hover table-striped" style="background-color:#FFF;font-family: 'Teko',sans-serif;">
  <tr class="active">
    <th style="text-align:center">Comment No</th>
    <th style="text-align:center">Sender</th>
    <th style="text-align:center">Comment</th>
    <th style="text-align:center">Date</th>
    <th style="text-align:center">Reply</th>
    <th style="text-align:center">Delete</th>
  </tr>
<?php 

    
    // $sql1="select * from notification where post_usr='$uid' order by noti_id DESC ";
    // $re=mysqli_query($con,$sql1);
    $sql="select * from notification where post_usr=:uid order by noti_id DESC";
    $query=$connect->prepare($sql);
    $query->bindParam(":uid",$comp_id);
    $query->execute();
    $count=$query->rowCount();
    $i=1;
    while($row=$query->fetch())
    {
        $from_app=$row["from_appl"];
        $no_id=$row["noti_id"];
        $po_id=$row["pos_id"];
        $post_user_id=$row["post_usr"];
        $comment_user_id=$row["comm_user"];
        $co=$row["comment"];
        $ti=$row["time"];
        echo '<tr class="default">';
    echo '<td style="text-align:center">'.$i.'</td>';
    $sql1="select * from registration where usr_id=:commented_by";
    $query1=$connect->prepare($sql1);
    $query1->bindParam(":commented_by",$comment_user_id);
    $query1->execute();
    while ($rows=$query1->fetch()) {
    echo '<td>'.$rows["name"].'</td>';
    echo '<td>'.$co.'</td>';
    echo '<td>'.$ti.'</td>';
    echo '<td><form method="post" action="reply.php">
    <input type="hidden" name="comment" value="'.$co.'" />
    <input type="hidden" name="po_id" value="'.$po_id.'" />
    <input type="hidden" name="commenter_id" value="'.$comment_user_id.'" />
    <input type="submit" name="reply" class="btn btn-success" value="Reply" id="reply"/>
    </form></td>';
    echo '<td><a href="javascript:delete_student('.$no_id.')" class="btn btn-danger" id="delete">delete</a></td>';
 echo '</tr>';  
}
}
?>
</table>
        </div>
    </div>
    <script>
  function delete_student(id)
  {
    if(confirm("You want to delete comment ?"))
    {
    window.location.href='execution.php?yesdelete='+id;
    }
  }
</script>
</body>
</html>