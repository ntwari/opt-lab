<?php
if(isset($_POST['submit'])){
    include("../includes/dbconfig_admin.php");
    $email=$_POST["email"];
    $password=$_POST["pass"];
    $sql="SELECT * FROM admins WHERE email=:email AND password=:password";
    $query=$connect->prepare($sql);
    $query->bindParam(":email",$email);
    $query->bindParam(":password",$password);
    if($query->execute()){
    $number_of_rows = $query->fetchColumn();
    if($number_of_rows>0){
        session_start();
        $_SESSION["email"] = $email;
        echo '<meta http-equiv="refresh" content="0;URL=admin-home.php" />';
    }
    else{
        $msg="Please fill with the expected credentials";
    }
}
else{
    $msg="Query not working now";
}
}
?>
<html>
   <head>
        <title>ADMIN SIGNUP</title>
        <link rel="stylesheet" href="styles/admin-style.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css?family=Saira+Condensed&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
   </head>
   <body>
       <section class="navbar">
       <h1><i class="fa fa-signal-stream"></i><span>OPT</span>Lab</h1>
       <ul>
           <li><a href="#">Home</a></li>
           <li><a href="#">About us</a></li>
           <li><a href="#">Login</a></li>
           <li><a href="#" id="actions">Sign up</a></li>
       </ul>
       </section>
       <section class="body">
       <section class="sidebar">
       </section>
       <section class="admin-form">
            <div class="headers">
                 <h1>LOGIN NOW</h1>
            </div>
            <?php 
          if(isset($msg)){
              echo '<div class="statusmsg">'.$msg.'</div>'; 
           } 
          ?>
            <div class="description">
                 <p>Continue with login in your dashboard:</p>
            </div>
            <form action="#" method="post">
            <div class="inputWithIcon">
            <i class="fas fa-envelope-open-text"></i>
            <input type="email" name="email" placeholder="Enter email" required>
            </div>

            <div class="inputWithIcon">
            <i class="fas fa-key"></i>
             <input type="password" placeholder="Enter your password" name="pass" required>
            </div>
            <input type="submit" name="submit" class="submit" value="login">
            </form>
            <p style="margin-left: 7em;font-size: 21px;">Are you not registered?<a href="admin-signup.php">register</a></p>
       </section>
       </section>
   </body>
</html>