<?php
   session_start();
   if($_SESSION['email']=='')
   {
      header('location:login.php');
   }
   include("../includes/dbconfig_admin.php");
?>
<?php
    $sql="SELECT * FROM admins WHERE email=:email";
    $query=$connect->prepare($sql);
    $query->bindParam(":email",$_SESSION["email"]);
    $query->execute();
    while ($row=$query->fetch()) {
        $comp_id=$row["comp_id"];
    }
?>
<?php
    $sql="SELECT * FROM admins WHERE email=:email";
    $query=$connect->prepare($sql);
    $query->bindParam(":email",$_SESSION["email"]);
    $query->execute();
    while ($row=$query->fetch()) {
        $admin_id=$row["admin_id"];
        $comp_id=$row["comp_id"];
        $name=$row["admin_first_name"]." ".$row["admin_last_name"];
        $email=$row["email"];
        $profile=$row["profile"];
        $national_id=$row["national_id"];
        $phone=$row["phone"];
        if(!$profile){
            $profile="user-1.jpg";
          }
        $sql1="SELECT * FROM company WHERE comp_id=:comp_id";
        $query1=$connect->prepare($sql1);
        $query1->bindParam(":comp_id",$comp_id);
        $query1->execute();
        while ($rows=$query->fetch()) {
            $comp_name=$rows["comp_name"];
        }
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles/applicants.css">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <title>Admin | Applicants</title>
</head>
<body>
    <div class="nav">
        <div class="logo">
            <a href="">
                <p><span>OPT</span>Lab</p>
            </a>
        </div>
        <div class="nav-buttons">
            <ul>
                <li><a href="admin-home.php">Home</a></li>
                <li><a href="#">Manage-Account</a></li>
                <li><a href="signout.php">Log Out</a></li>
            </ul>
        </div>
    </div>
    <div class="mainbody">
        <div class="functionalities">
            <div class="admin-functionalities">
            <?php 
                 echo ' <img src="images/'.$profile.'" width="300px" height="300px">';
                        ?><br>
            </div>
                <div class="functionality-menu">
                <ul>
                        <li> <a href="admin-home.php">Dash Board</a> </li>
                        <li> <a href="comp.php">Company Overview</a> </li>
                        <li> <a href="applicants.php">All Applicants</a> </li>
                        <li class="active-service"> <a href="voted_applicants.php">Voted Applicants</a> </li>
                        <li> <a href="interview-room.php">Interview room</a> </li>
                        <li> <a href="notification.php">Notifications</a> </li>
                        <li> <a href="admin-login.php">Log out</a> </li>
                    </ul>
            </div>
        </div>
        <div class="functionality-desc">
        <div class = 'container applicants'>
                        <table class = 'table table-bordered table-striped table-hover'>
                        <thead class = 'thead-dark'>
                            <tr>
                                <th>#</th>
                                <th>Full name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
            <?php
            include("../includes/dbconfig_admin.php");
                      # <div class = 'text-center' id = 'status'></div>
          $status=2;
          $sql="SELECT * FROM application WHERE manager=:company AND status=:status";
          $query=$connect->prepare($sql);
          $query->bindParam(":company",$comp_id);
          $query->bindParam(":status",$status);
          $query->execute();
          $number_available=1;
          $i=1;
          $number_of_rows = $query->rowCount();
         echo "<p class='applicants'>Allowed applicants :".$number_of_rows."</p>";
          while ($row=$query->fetch()) {
              $owner=$row["manager"];
              $applicant=$row["applicant"];
            echo "
            <tr>
               <td>".$i."</td>
               <td>".$row["full_name"]."</td>
               <td>".$row["email"]."</td>
               <td>".$row["phone"]."</td>
               <td class='open_cv'><a class='btn btn-info' href='video_chat/index.php?admin_id=".$owner."&appl_id=".$applicant."' target='_blank'>Join Interview</a></td>
        ";
        $i++;
          }
        //   <button class="button-applicant"><a href="video_chat/index.php?admin_id='.$owner.'&appl_id='.$applicant.'" target="_blank">Interview</a></button>
            ?>
        </div>
    </div>
</body>
</html>