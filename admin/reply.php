<?php
   session_start();
   if($_SESSION['email']=='')
   {
      header('location:admin-login.php');
   }
   include("../includes/dbconfig_admin.php");
?>
<?php 
 if(isset($_POST["submit"]))
 { 
    $post_name=$_POST["post_name"];
    $post_desc=$_POST["post_desc"];
    $current_date=date("Y-m-d");
    $company_id=$_POST["company_id"];
    $sql="INSERT INTO post(comp_id,career_name,career_desc,posted_date) VALUES(:company,:name,:desc,:current_date) ";
    $query=$connect->prepare($sql);
    $query->bindParam(":company",$company_id);
    $query->bindParam(":name",$post_name);
    $query->bindParam(":desc",$post_desc);
    $query->bindParam(":current_date",$current_date);
    if($query->execute()){
             echo "
     <script>
        alert('yes done now');
     </script>
     ";
    }
    else{
             echo "
     <script>
        alert('Not done now done now');
     </script>
     ";
    }
 }
?>
<?php
    $sql="SELECT * FROM admins WHERE email=:email";
    $query=$connect->prepare($sql);
    $query->bindParam(":email",$_SESSION["email"]);
    $query->execute();
    while ($row=$query->fetch()) {
        $admin_id=$row["admin_id"];
        $comp_id=$row["comp_id"];
        $name=$row["admin_first_name"]." ".$row["admin_last_name"];
        $email=$row["email"];
        $profile=$row["profile"];
        $national_id=$row["national_id"];
        $phone=$row["phone"];
        if(!$profile){
            $profile="user-1.jpg";
          }
        $sql1="SELECT * FROM company WHERE comp_id=:comp_id";
        $query1=$connect->prepare($sql1);
        $query1->bindParam(":comp_id",$comp_id);
        $query1->execute();
        while ($rows=$query->fetch()) {
            $comp_name=$rows["comp_name"];
        }
    }
?>
<?php 
   include("../includes/dbconfig_admin.php");
?>
<?php
$sql="SELECT * FROM `registration` WHERE `email`=:email";
$query=$connect->prepare($sql);
$query->bindParam(":email",$_SESSION["email"]);
      if ($query->execute())
         {    
          while($row = $query->fetch()) 
          {     
        $image=$row['image'];
        }
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="styles/comp.css">
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
    <title>company overview</title>
    <link href="https://fonts.googleapis.com/css?family=Saira+Condensed&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://kit.fontawesome.com/a076d05399.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</head>
<body>
    <div class="nav">
        <div class="logo">
            <a href="">
                <p><span>OPT</span>Lab</p>
            </a>
        </div>
        <div class="nav-buttons">
            <ul>
                <li><a href="home.php">Home</a></li>
                <li><a href="#">Manage-Account</a></li>
                <li><a href="signout.php">Log Out</a></li>
            </ul>
        </div>
    </div>
    <div class="mainbody">
        <div class="functionalities">
            <div class="admin-functionalities">
            <?php 
                 echo ' <img src="images/'.$profile.'" width="300px" height="300px">';
                        ?><br>
            </div>
                <div class="functionality-menu">
                <ul>
                        <li> <a href="admin-home.php">Dash Board</a> </li>
                        <li class="active-service"> <a href="comp.php">Company Overview</a> </li>
                        <li> <a href="applicants.php">All Applicants</a> </li>
                        <li> <a href="voted_applicants.php">Voted Applicants</a> </li>
                        <li> <a href="interview-room.php">Interview room</a> </li>
                        <li> <a href="notification.php">Notifications</a> </li>
                        <li> <a href="admin-login.php">Log out</a> </li>
                    </ul>
            </div>
        </div>
        <div class="functionality-desc">
        <form action="../user-1/comment1.php" method="post">
            <div class="post-footer">
                <div class="input-group"> 
                    <textarea class="form-control" cols="50" rows="8" placeholder="Add a comment" type="text" name="comment"></textarea>
                    <input class="form-control"  type="hidden" name="uid" value="<?php echo $_SESSION['id']; ?>">
                    <input class="form-control"  type="hidden" name="pid" value="<?php echo $p_id;?>">
                    <input class="form-control"  type="hidden" name="from_appl" value="1">
                    <input class="form-control"  type="hidden" name="comme_id" value="<?php echo $c_id;?>">
                    <div class="btn-group" role="group" style="margin-top:5px">
                    <button type="submit" name="submit_comment" class="button">Comment  </button>
                </div>
                </div>
                </form>
        </div>
    </div>
</body>
</html>