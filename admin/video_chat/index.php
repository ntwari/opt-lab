<?php 
$admin=$_GET["admin_id"];
$applicant_id=$_GET["appl_id"];

include("../../includes/dbconfig_admin.php");
// $query="SELECT * FROM registration WHERE usr_id=".$admin." ";
$sql="SELECT * FROM registration WHERE usr_id=:applicant";
$query=$connect->prepare($sql);
$query->bindParam(":applicant",$applicant_id);
$query->execute();
while($row=$query->fetch()){
  $applicant_images=$row["image"];
  $applicant_name=$row["name"];
    // $query1="SELECT * FROM registration WHERE usr_id=".$applicant_id."";
    $sql1="SELECT * FROM admins WHERE comp_id=:admin_id";
    $query1=$connect->prepare($sql1);
    $query1->bindParam(":admin_id",$admin);
    $query1->execute();
    while($row1=$query1->fetch()){
      $admin_image=$row1["profile"];
      $admin_name=$row1["admin_first_name"]." ".$row1["admin_last_name"];
      if(!$admin_image){
        $admin_image="user-1.jpg";
      }
    }
}
?>
<?php 
if(isset($_POST["save_button"])){
$comment_on_interview=$_POST["interview"];
$applicant_id=$_GET["appl_id"];

$add_interview_query = "";
$add_interview_query="UPDATE application SET interview =:interview WHERE applicant=:applicant ";
$query3=$connect->prepare($add_interview_query);
$query3->bindParam(":interview",$comment_on_interview);
$query3->bindParam(":applicant",$applicant_id);
if($query3->execute()){
    echo '
    <script>
    confirm("The notes about the interview of '.$applicant_name.' are saved..");
    </script>
    ';
}
else if(!$query3){
    echo '
    <script>
    alert("The notes are not saved..");
    </script>
    ';
}
}
?>
<!DOCTYPE HTML>
<html>
<head> 
        <meta charset=utf-8>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script language="JavaScript" type="text/javascript" src="//comet-server.com/js/lib/jquery.min.js" ></script>
        <script src="//comet-server.com/CometServerApi.js" type="text/javascript"></script>
        <script src="//comet-server.com/cometVideoApi.js" type="text/javascript"></script> 
  <link rel="stylesheet" type="text/css" href="../css1/manager.css">
  <link rel="stylesheet" type="text/css" href="style.css">
  <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Tangerine">
          <link href="https://fonts.googleapis.com/css?family=Allerta+Stencil|Black+Ops+One|Fredericka+the+Great|Nanum+Pen+Script|Oleo+Script|Satisfy&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Palanquin&display=swap" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <link href="https://fonts.googleapis.com/css?family=Khand|Palanquin|Viga|Yantramanav&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Barlow+Condensed&display=swap" rel="stylesheet">
</head>
<style>
.manage_title h1{font-family:'Khand', sans-serif;margin-left: 13em}
.user_id{margin-left: 33em}
button:hover{background: #d35400;transition: ease-in-out 0.4s;cursor: pointer;}
#mute{border:none;border-radius: 5px;height: 3em;width: 3em;}
#mute i{font-size: 2em;outline: none}
#mute:hover{color: #fff;cursor: pointer;}
.open_book{float: left;}
.open_book button{outline: none;}
.open_book button:hover{background: #27ae60;}
.sidenav_index {
	height: 100%;width: 0;position: fixed;z-index: 1;top: 0;right: 0;background: #fff;overflow-y: hidden;transition: 0.5s;padding-top: 60px;text-align:center;border-left: 10px solid #27ae60;}
  
  .sidenav_index a {
	padding: 8px 8px 8px 32px;text-decoration: none;font-size: 25px;color: #818181;display: block;transition: 0.3s; }
  
  .sidenav_index a:hover{
	color: #f1f1f1; }
  
  .sidenav_index .closebtn {
	position: absolute;top: 0;right: 25px;font-size: 36px;margin-left: 50px;
  }
  
  @media screen and (max-height: 450px) {
.sidenav_index {padding-top: 15px;}
.sidenav_index a {font-size: 18px;}
  }
.closebtn:hover{background: linear-gradient( to right,#2ecc71,#2ecc71,#2ecc71);border: none;border-radius: 50%;}
.design{background: linear-gradient(-90deg,#2ecc71,#93f1ba,#3cfa8b););height: 80%;margin: 1em;-webkit-box-shadow: inset 14px 6px 23px -2px rgba(0,0,0,0.61);-moz-box-shadow: inset 14px 6px 23px -2px rgba(0,0,0,0.61);box-shadow: inset 14px 6px 23px 2px rgba(0,0,0,0.61);}
.resume{background: linear-gradient(90deg,white,#bdc3c7);height: 50%;box-shadow: -113px -85px 78px -60px rgba(0,0,0,0.5);}
.resume h1{font-family:'Khand', sans-serif;color: #34495e;text-transform: uppercase;}
.design textarea{border: 1px solid #34495e;outline: none;border-radius: 10px;margin-top: 3px;font-family: 'Barlow Condensed', sans-serif;font-size: 1.2em;padding: 1em}
.submit_button{	background-color: #34495e;border-radius:28px;border:1px solid #18ab29;display:inline-block;cursor:pointer;color:#ffffff;font-family:Arial;font-size:17px;padding:16px 76px;text-decoration:none;text-shadow:0px 1px 0px #2f6627;}
.submit_button:hover {
	background-color: #7f8c8d;transition: ease-in-out 0.4s;
}
.myButton:active {
	position:relative;top:1px;
}
.headers{display: flex;flex-wrap: wrap;background: #34495e;color: #fff;margin: -1em;padding: 1em;border-bottom: 1em solid #27ae60;}
.desc{font-family:'Khand', sans-serif;color: #fff;}
</style>
<body>
 <section class="headers" id="otherpart">
   <?php echo '<img style="width: 70px;height: 60px;border: none;border-radius: 50%;" src="../../user/user_images/'.$applicant_images.'"';?><br>
     <h1  class="desc">Interviewee</h1>
     <!-- What are the differences between a Interviewee and a Interviewer -->
     <div class="manage_title" style="margin-right: 24em;">
       <h1>INTERVIEW DASHBOARD</h1>
     </div>
        <?php echo '<img style="width: 70px;height: 60px;border: none;border-radius: 50%;" src="../images/'.$admin_image.'"';?><br>
        <h1 class="desc">Interviewer</h1>
   </section>
   <div class="status" style="margin-top: 1em;margin-bottom: 1em;font-family: 'Barlow Condensed', sans-serif;font-size: 2em;">
        We are waiting for authorization on Inteview 
    </div>
<div class="open_book">
<button id="mute" onclick="openNav()"><i class="fa fa-book" aria-hidden="true"></i></button>
</div>
<div class="root">
    <div>
         <video id="video_remote" autoplay="autoplay" ></video>
         <video id="video_local" autoplay="autoplay" muted="true" ></video>
         <audio id="audio_remote" autoplay="autoplay" style="display: none;"></audio>
    </div>
    <hr> 
    <div class="callbtns">
        <button class="hideincall StartCallBtn" onclick="StartCall()" style="background: #34495e;padding: 1em;border: none;border-radius: 5px;color: #fff;font-size: 1em;">Join Interview</button>
        <button class="showincall EndCall" onclick="EndCall()" style="background: #34495e;padding: 1em;border: none;border-radius: 5px;color: #fff;font-size: 1em">Hangup</button>
        <button id="mute" class="showincall MuteAudio" onclick="MuteAudio()"><i class="fa fa-microphone-slash" aria-hidden="true"></i></button>
        <button id="mute" class="showincall MuteVideo" onclick="MuteVideo()"><i class="fa fa-stop-circle-o" aria-hidden="true"></i></button>
    </div>
</div>

<div id="mySidenav" class="sidenav_index">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <div class="design">
     <div class="resume">
        <h1>BOOKLET FOR <?php echo $applicant_name?></h1>
        <?php echo '<img style="width: 130px;height: 130px;border: none;border-radius: 50%;" src="../../user/user_images/'.$applicant_images.'"';?>
     </div>
     <h1>WRITE BELOW ABOUT THE INTERVIEW</h1>
  </div>
  <div>
      <form method="post">
      <textarea name="interview" cols="60" required rows="7"></textarea>
      <button class="submit_button" name="save_button">Save Note <i class="fa fa-paper-plane" aria-hidden="true"></i></button>
      </form>
  </div>
</div>

<script>
function openNav() {
  document.getElementById("mySidenav").style.width = "50%";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
}
</script>
<script>

$("document").ready(function(){
  $("#notification").click(function(){
    $("#contact").slideToggle("slow");
  });
});
</script>

<script type="text/javascript">
 function log()
{
    console.log(arguments);
    $("#log").append(JSON.stringify(arguments)+"<hr>");
}
function StartCall()
{ 
    $(".StartCallBtn").hide();
    $(".status").html("We are waiting for permission to call");
    roomId = "101";
    log("call", roomId)
    // Connecting to video chat
    $.ajax({
        url: "https://comet-server.com/doc/CometQL/videochat/call.php?user_id="+cometApi.getUserId()+"&room="+roomId,
        type: "POST", 
    });
}
function EndCall()
{ 
    // End the call
    cometVideoApi.hangup();
    window.stop();
}
function MuteAudio()
{
    cometVideoApi.muteAudio(!cometVideoApi.isMute().audio)
}
function MuteVideo()
{
    cometVideoApi.muteVideo(!cometVideoApi.isMute().video)
}
// Authorization on comets server
function auth()
{ 
    var user_id = localStorage['user_id']
    if(!user_id)
    {
        user_id = Math.floor(Math.random()*1000000)
        localStorage['user_id'] = user_id 
    }
    
    log("Request authorization for user_id="+user_id)
    var user_key = "PassForUser"+user_id
    
    // Connecting to video chat
    return $.when($.ajax({
        url: "https://comet-server.com/doc/CometQL/videochat/auth.php?user_id="+user_id+"&user_key="+user_key,
        type: "GET",
        dataType:'json', 
    })).always(function(res){ 
            $(".status").html("We are waiting for connect to the server");
            cometApi.start({
                node: "app.comet-server.ru",
                dev_id: 15,
                user_id:user_id,
                user_key:user_key,
            }) 
        }).promise();
}
 
cometApi.onAuthSuccess(function()
{
        // We will initialize the call after successful authorization on the comet server.
        $(".StartCallBtn").removeClass("hide");
        log("Authorization on the server is complete")
     $(".status").html("Authorization on the server is complete");
})
 
auth();
 
// We initiate the activation of cometVideoApi
cometVideoApi.start({ 
    // Callback called before starting the connect for the call
    // It is assumed that it will set the settings for the next call
    // Such as the parameters audio_remote, video_local, video_remote and maybe even some more.
    // And then the cometVideoApi.acceptCall (event) function will be called
    // And if it's not called, then we did not pick up the phone.
    onCall:function(callEvent)
    {
        $(".status").html("Incoming call, press answer.");
        if(!confirm("Answer a call?"))
        {
            // We decided not to answer the call
            $(".status").html("We decided not to answer the call");
            $(".StartCallBtn").show();
            return
        }
        // Receive call
        cometVideoApi.acceptCall({
            // Type of call 'audio' | 'video'
            type:'video',
            // Specify the target element for the video stream from the interlocutor
            video_remote: $("#video_remote")[0],
            // Specify the target element for the audio stream from the interlocutor
            audio_remote: $("#audio_remote")[0],
            // Specify the target element for the video stream from me (my own image from the camera)
            video_local: $("#video_local")[0],
        })
        $(".status").html("We look forward to hearing from An interviewee");
    },
    
            /**
              * Callback end of call
              * @param {object} event
              * {
              * Action: "",         // Event name
              * Status: "",         // Call completion reason
              * CallInfo: {},     // Information about the call
              * Time: 1000            // Time duration of the call
              * Type: "audio"     // Type of call
              * }
              */
    onCallEnd:function(event)
    {
        $(".root").removeClass("incall")
        $(".StartCallBtn").show();
        $("video").css("background-color", "black")
        $(".status").html("Interview is ended. Thank you all");
        log("onCallEnd", event)
    },
    
            /**
              * Callback picking up the interlocutor
              * @param {object} event
              * {
              * Action: "accept", // Event name
              * CallInfo: {},         // Information about the call
              * Type: "audio"         // The type of call that is selected by the interlocutor
              * }
              *
              * The callback is called only once for the first responding interlocutor
              *
              */
    onCallAccept: function(event)
    { 
        $(".status").html("Received a response from another participant in the conference.");
        log("onAccept", event)
    },
    
            /**
              * Callback when I and my interlocutor are connected to the server
              * @param {object} event
              * {
              * Action: "start",    // Event name
              * CallInfo: {},         // Information about the call
              * Type: "audio"         // Type of call
              *}
              */
    onCallStart: function(event)
    {
        $(".status").html("connect to the media server is complete. The conversation began.");
        $(".root").addClass("incall")
        log("onCallStart", event)
    },
    onOut: function(event)
    {
        // Participant's exit from the conference
        $(".status").html("One of the interlocutors left the conference");
        log("onOut", event)
    },
    onIn: function(event)
    {
        // Log in to the conference
        $(".status").html("Another person joined the conference.");
        log("onIn", event)
    },
})
</script>
</body>
</html>