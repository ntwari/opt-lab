<?php 
    include("dbconfig.php");
?>
<?php
  session_start();
  if($_SESSION['name']=='')
  {
     header('location:signin.php');
  }

 ?>
<?php
function timeAgo($time_ago){

			$time_ago = strtotime($time_ago);
			$cur_time   = time();
			$time_elapsed   = $cur_time - $time_ago;
			$seconds    = $time_elapsed ;
			$minutes    = round($time_elapsed / 60 );
			$hours      = round($time_elapsed / 3600);
			$days       = round($time_elapsed / 86400 );
			$weeks      = round($time_elapsed / 604800);
			$months     = round($time_elapsed / 2600640 );
			$years      = round($time_elapsed / 31207680 );
			// Seconds
			if($seconds <= 60){
			    return "just now";
			}
			//Minutes
			else if($minutes <=60){
			    if($minutes==1){
			        return "one minute ago";
			    }
			    else{
			        return "$minutes minutes ago";
			    }
			}
			//Hours
			else if($hours <=24){
			    if($hours==1){
			        return "an hour ago";
			    }else{
			        return "$hours hrs ago";
			    }
			}
			//Days
			else if($days <= 7){
			    if($days==1){
			        return "yesterday";
			    }else{
			        return "$days days ago";
			    }
			}
			//Weeks
			else if($weeks <= 4.3){
			    if($weeks==1){
			        return "a week ago";
			    }else{
			        return "$weeks weeks ago";
			    }
			}
			//Months
			else if($months <=12){
			    if($months==1){
			        return "a month ago";
			    }else{
			        return "$months months ago";
			    }
			}
			//Years
			else{
			    if($years==1){
			        return "one year ago";
			    }else{
			        return "$years years ago";
			    }
			}
		} 
?>

<?php 
$sql="SELECT * FROM `registration` WHERE `name`='".$_SESSION['name']."'";
	$result = $con->query($sql);
			if ($result->num_rows>0)
				 {    
			    while($row = $result->fetch_assoc()) 
			    {	    
		$image=$row['image'];
		$applicant=$row["usr_id"];
}
}
 ?>


<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta name="description" content="">
 <meta name="author" content="">
 <link rel="icon" href="../../favicon.ico">
<title>Job Post Pannel</title>

 <script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="components/bootstrap/dist/js/jquery.js"></script>
 <link href="components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
 <!-- Custom styles for this template -->
 <link href="components/bootstrap/dist/css/simple-sidebar.css" rel="stylesheet">
  <link href="components/bootstrap/dist/css/postmodal.css" rel="stylesheet">
  <link href="components/bootstrap/dist/css/fbbox.css" rel="stylesheet">
   <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Tangerine">
<link href="https://fonts.googleapis.com/css?family=Teko&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Patua+One|Teko&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/css?family=Barlow+Condensed&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Khand|Palanquin|Viga|Yantramanav&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
</head>
<style type="text/css">
	body{ font-family: Dosis !important; font-size: 1.3em; line-height: 40px;}
	.navbar{
		background: #34495e;border-bottom: 1em solid #2980b9;color: #ffff;height: 90px;}
	.toggled{
		background-color: #34495e;}
	#menu-toggle:hover{
       background-color: #7f8c8d;height: 100%;width: 120%;border: none;border-radius: 1em;}
	#sidebar-wrapper{background-color: #ecf0f1;	margin-top: 3em;border-right: 12px solid #27ae60;font-family:'Khand', sans-serif;font-size: 1.3em;font-weight: 100;}
	.list-group a:hover{background-color: #34495e;   }
    img{     height: 50px;   border: none;    border-radius: 50%;   }
    .post-show{   	box-shadow: 2px 4px 4px #7f8c8d;  }
    .post-show:hover{	box-shadow: 5px 13px 13px #7f8c8d; }
    .post-show .btn-default{background-color: #e67e22;color: #fff;height: 3em;text-align: center;font-family: 'roboto';}
    .post-show .btn-defaul:hover{	box-shadow: 2px 4px 4px #7f8c8d;background-color: #7f8c8d; } 
    .post-show .apply{color: #fff;}
    .post-show .aplly{
       background-color: #7f8c8d;width: 20%;text-align: center;border: none;border-radius: 10px;float: right; font-family: 'Teko',sans-serif}
    .getIn{font-family: 'Teko',sans-serif;font-size: 4em;	margin-top: 2em;text-align: center;color: #e67e22;}
    .pages a{color: #fff;text-align: center;}
	    .pages{	background-color:#2c3e50;font-size: 1em;margin-bottom: 2em;	height: 2em;width: 3em;	float: left;text-align: center;border: none;border-radius: 30%;margin-right: 5em;}
    .pages:hover{box-shadow: 2px 4px 4px #7f8c8d;}
	.img{margin-left: 97em;margin-top: -8em}
	.side-nav{margin-top: 5em;}
	.sidebar-nav li a{background: #34495e;font-family:'Khand', sans-serif;margin-top: 1em}
	.profile img{margin-top: 3em;margin-left: 2em;height: 120px;width:  120px;border: 1px solid #27ae60;}
	.name{text-align: center;font-family:'Khand', sans-serif;color: #e67e22;margin-left: -1em;font-size: 1.4em;}
	.links ul li{background: #34495e;margin-top: 1em;cursor: pointer}
	.links ul li:hover{background: #2ecc71;transition: ease-in-out 0.5s;}
	.links ul li a{text-decoration: none;font-family:'roboto';margin-left: 2em;padding-bottom: 0.4em;}
	.links ul li a i{color: white;font-size: 2em;padding-right: 0.4em;padding-top: 0.4em}
	/* others for users  */
	.id-img-box{height: 150px;width:150px;margin-bottom: 3em;margin-right: 1em}
	.img1{border: 1px solid #27ae60;width: 110px;height: 110px}
	.name{font-family: 'Khand', sans-serif;font-size: 28px;line-height: 35px;color: #000000;padding-left: 2em}
	.time-counter{padding-left: 2em;font-weight: 900;color: #95a5a6;font-family: 'Khand', sans-serif;}
	.post-title{font-family: 'Teko',sans-serif;font-size: 2em;margin-top: 2em}
	.post-header-text{margin-top: 5em}
	.status{font-family: 'Khand', sans-serif;font-weight: 100;color: black;font-size: 2em;}
 .comments{font-family: 'Khand', sans-serif;font-weight: 100;color: #27ae60;font-size: 1.4em;margin-left: 4em;margin-top: -2em}
 .comment-by{color: #e67e22;font-family: 'Khand', sans-serif;font-size: 2em;text-transform: uppercase;}
 .myButton {
	box-shadow: -11px 17px 18px -7px #276873;
	background:linear-gradient(to bottom, #599bb3 5%, #408c99 100%);
	background-color:#599bb3;
	border-radius:23px;
	border:2px solid #29668f;
	display:inline-block;
	cursor:pointer;
	color:#ffffff;
	font-family:Arial;
	font-size:16px;
	font-weight:bold;
	padding:4px 16px;
	text-decoration:none;
	text-shadow:0px 1px 0px #3d768a;
}
.myButton:hover{text-decoration: none;color: #fff;}
.myButton:active .myButton:hover {
	position:relative;
	top:1px;
	background:linear-gradient(to top, #599bb3 5%, #408c99 100%);
	transition: ease-in-out 0.5s;
}
#like-icon,#like-icon-b{font-size: 1.5em;background: #95a5a6;padding: 10px;border: none;border-radius: 30%;color: #fff;cursor: pointer;margin-right: 0.5em}
#like-icon:hover{transition: ease-in-out 0.4s;background: #34495e;transform: rotate(-30deg)}
#like-icon-b:hover{transition: ease-in-out 0.4s;background: #34495e;transform: rotate(30deg)}
</style>
<body>

 <h1 class="getIn">Get in Touch with Job Available</h1>
 	     <div class="col-lg-8" style="margin-left: 28em;margin-top:2em;">
						<?php 
						$num_rec_per_page=10;
						if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; }; 
                          $start_from = ($page-1) * $num_rec_per_page; 
                            $sql=mysqli_query($con,"select * from post join company where post.comp_id=company.comp_id order by post.posted_date desc limit  $start_from, $num_rec_per_page");
                             while($row=mysqli_fetch_array($sql))
                               {
								$proj_name=$row["career_name"];
								$project=$row["post_id"];
     	                           $id=$row["post_id"];
     	                           $like=$row["like"];
     	                           $dislike=$row["unlike"];
     	                           $time=$row["posted_date"];

						echo '<div class="post-show" style="width:90%;border-radius:5px">
									<div class="post-show-inner">
										<div class="post-header">
											<div class="post-left-box">
												<div class="id-img-box"><a href="otheruser.php?userid='.$row['post_id'].'">
                                                 </div>
													<ul>
													<b><a href="otheruser.php?userid='.$row['comp_id'].'" class="name">	'.$row['comp_name'].'</a> </b>
														<li class="time-counter">'.timeAgo($time).'</li>
													</ul>
												</div>
											</div>
											<div class="post-right-box"></div>
										</div>
									
											<div class="post-body">
							<div class="post-header-text">
							 <a href="project_description.php?id='.$id.'&s_title=' . $row['career_desc'] . '" class="post-title">'.$row['career_name'].'</a>
							  
							                 <p class="status">'.$row['career_desc'].'</p>

											';
										$sql1=mysqli_query($con,"select * from comments where post_id_c='$id'");
							
										while($row1=mysqli_fetch_array($sql1))
										{	 
                                            $ct=$row1["comment_time"];
                                            $c=$row1["comment"];
                                            $uid=$row1["user_id_c"];
                                            $sql2=mysqli_query($con,"select * from registration where usr_id='$uid'");
                                            while($row2=mysqli_fetch_array($sql2))
										     {
                                                 $n=$row2["name"];
												 $img=$row2["image"];
                                                 echo '<div style="margin-left:50px;">
										<a href="otheruser.php?userid='.$uid.'"><img style="height:40px; width="40px" src="user_images/'.$img.'"></img></a>
										    <p class="comments">'.$c.'</p>
											 </div>
											 <div style="margin-left:50px"><div class="id-name">
													<ul style="color: #7f8c8d;font-size: 1em;font-weight: 100;">
														<small>'.timeAgo($ct).'</small> &nbsp; &nbsp; &nbsp;<font style="color:#2c3e50;font-family: roboto "> comment by :</font> <font style="font-size:12px"><a href="otheruser.php?userid='.$uid.'" class="comment-by"> '.$n.'</a></font>
													</ul>
											</div>
										</div>
										
										';
										     }
									   }
							echo '<a href="project_description.php?id='.$id.'&s_title=' . $row['career_desc'] . '" class="myButton">Post Your Comment</a>
                        
                                <div class="aplly">
                                    <a href="application.php?applicant='.$applicant.'&company='.$row["comp_id"].'&post='.$row["post_id"].'" class="apply"> Apply now
                                </div>
							</div>
        
								</div>
								</div></div><br/> ';	
     	                           
                               }                    
?>                   
</div>
</div> 
</div>
</div>
<script src="components/bootstrap/dist/js/bootstrap.min.js"></script><script>
 $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        // $("toggled").css('background-color','#34495e');
        // $("#wrapper").css('background-color','#34495e');
    });
</script>


	<script type="text/javascript">
		$(document).ready(function(){
			$('.status').click(function() { $('.arrow').css("left", 0);});
			$('.photos').click(function() { $('.arrow').css("left", 146);});
		});
	</script>
</body>
</html>