<?php 
    include("dbconfig.php");
?>
<?php
  session_start();
  if($_SESSION['name']=='')
  {
     header('location:reg.php');
  }

 ?>
<?php
function timeAgo($time_ago){

			$time_ago = strtotime($time_ago);
			$cur_time   = time();
			$time_elapsed   = $cur_time - $time_ago;
			$seconds    = $time_elapsed ;
			$minutes    = round($time_elapsed / 60 );
			$hours      = round($time_elapsed / 3600);
			$days       = round($time_elapsed / 86400 );
			$weeks      = round($time_elapsed / 604800);
			$months     = round($time_elapsed / 2600640 );
			$years      = round($time_elapsed / 31207680 );
			// Seconds
			if($seconds <= 60){
			    return "just now";
			}
			//Minutes
			else if($minutes <=60){
			    if($minutes==1){
			        return "one minute ago";
			    }
			    else{
			        return "$minutes minutes ago";
			    }
			}
			//Hours
			else if($hours <=24){
			    if($hours==1){
			        return "an hour ago";
			    }else{
			        return "$hours hrs ago";
			    }
			}
			//Days
			else if($days <= 7){
			    if($days==1){
			        return "yesterday";
			    }else{
			        return "$days days ago";
			    }
			}
			//Weeks
			else if($weeks <= 4.3){
			    if($weeks==1){
			        return "a week ago";
			    }else{
			        return "$weeks weeks ago";
			    }
			}
			//Months
			else if($months <=12){
			    if($months==1){
			        return "a month ago";
			    }else{
			        return "$months months ago";
			    }
			}
			//Years
			else{
			    if($years==1){
			        return "one year ago";
			    }else{
			        return "$years years ago";
			    }
			}
		} 
?>
<?php 
$sql="SELECT * FROM `registration` WHERE `name`='".$_SESSION['name']."'";
  $result = $con->query($sql);
      if ($result->num_rows>0)
         {    
          while($row = $result->fetch_assoc()) 
          {     
        $image=$row['image'];
}
}
 ?>
<!DOCTYPE html>
<html lang="en">
<head>
 <meta charset="utf-8">
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <meta name="viewport" content="width=device-width, initial-scale=1">
 <meta name="description" content="">
 <meta name="author" content="">
 <link rel="icon" href="../../favicon.ico">
 
 <!-- http://draganzlatkovski.com/code-projects/toggle-jquery-side-bar-menu-in-bootstrap-free-template/ -->
 
 <title>Job Post Pannel</title>

 <script src="http://code.jquery.com/jquery-latest.js"></script>
<script src="components/bootstrap/dist/js/jquery.js"></script>
 <link href="components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
 <!-- Custom styles for this template -->
 <link href="components/bootstrap/dist/css/simple-sidebar.css" rel="stylesheet">
  <link href="components/bootstrap/dist/css/postmodal.css" rel="stylesheet">
  <link href="components/bootstrap/dist/css/fbbox.css" rel="stylesheet">
   <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Tangerine">
<link href="https://fonts.googleapis.com/css?family=Teko&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Patua+One|Teko&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="style.css">
<link href="https://fonts.googleapis.com/css?family=Barlow+Condensed&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Khand|Palanquin|Viga|Yantramanav&display=swap" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
</head>
<style type="text/css">
	body{ font-family: Dosis !important; line-height: 40px;background: #E5E5E5;}
	.navbar{
		background: #34495e;border-bottom: 1em solid #2980b9;color: #ffff;height: 90px;}
	.toggled{
		background-color: #34495e;}
	#menu-toggle:hover{
       background-color: #7f8c8d;height: 100%;width: 120%;border: none;border-radius: 1em;}
	#sidebar-wrapper{background-color: #ecf0f1;	margin-top: 3em;border-right: 12px solid #27ae60;font-family:'Khand', sans-serif;font-size: 1.3em;font-weight: 100;}
	.list-group a:hover{background-color: #34495e;   }
    img{height: 50px;   border: none;    border-radius: 50%;   }
    .post-show{   	box-shadow: 2px 4px 4px #7f8c8d;  }
    .post-show:hover{	box-shadow: 5px 13px 13px #7f8c8d; }
    .post-show .btn-default{background-color: #e67e22;color: #fff;height: 3em;text-align: center;font-family: 'roboto';}
    .post-show .btn-defaul:hover{	box-shadow: 2px 4px 4px #7f8c8d;background-color: #7f8c8d; } 
    .post-show .apply{color: #fff;}
    .post-show .aplly{
       background-color: #7f8c8d;width: 20%;text-align: center;border: none;border-radius: 10px;float: right; font-family: 'Teko',sans-serif}
    .getIn{font-family: 'Teko',sans-serif;font-size: 4em;	margin-top: 2em;text-align: center;color: #e67e22;}
    .pages a{color: #fff;text-align: center;}
	    .pages{	background-color:#2c3e50;font-size: 1em;margin-bottom: 2em;	height: 2em;width: 3em;	float: left;text-align: center;border: none;border-radius: 30%;margin-right: 5em;}
    .pages:hover{box-shadow: 2px 4px 4px #7f8c8d;}
	.img{margin-left: 90em;margin-top: -8em}
	.side-nav{margin-top: 5em;}
	.sidebar-nav li a{background: #34495e;font-family:'Khand', sans-serif;margin-top: 1em}
	.profile img{margin-top: 3em;margin-left: 2em;height: 120px;width:  120px;border: 1px solid #27ae60;}
	.name{text-align: center;font-family:'Khand', sans-serif;color: #e67e22;margin-left: -1em;font-size: 1.4em;}
	.links ul li{background: #34495e;margin-top: 1em;cursor: pointer}
	.links ul li:hover{background: #2ecc71;transition: ease-in-out 0.5s;}
	.links ul li a{text-decoration: none;font-family:'roboto';margin-left: 2em;padding-bottom: 0.4em;}
	.links ul li a i{color: white;font-size: 2em;padding-right: 0.4em;padding-top: 0.4em}
	.col-lg-8{margin-top: 10em;margin-left: 14em}
	.name{font-family:'Khand', sans-serif;font-size: 2em;text-align: left;color: black;font-weight: 900}
	.status{font-size: 1.4em;font-family:'Khand', sans-serif;margin-left: 8em}
	.status-1{font-size: 1.4em;font-family:'Khand', sans-serif;margin-top: 6em}
	.imgs{height: 120px}
	.time{font-family: 'roboto';color: #7f8c8d;}
</style>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
 <div class="container-fluid">
 <div class="navbar-header">
 <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false">

 </button>
 <a class="navbar-brand" href="#menu-toggle" id="menu-toggle"><span class="glyphicon glyphicon-list" aria-hidden="true"></span></a>
 </div>
 <div id="navbar" class="navbar-collapse collapse">
    <label class="navbar-text text-center text-primary" style="vertical-align:10px;"><font style="font-size:1.9em; color:white;font-family: 'Tangerine', serif;">universal Seek Job </font></label>
   <label class="navbar-text text-center text-primary" style="vertical-align:10px;font-size:medium ;color: #ffff"><font style="font-size:13px;font-family:'Khand', sans-serif;font-size: 1.5em;color: #ffff;padding-left: 12em;text-transform: uppercase;"> <?php echo $_SESSION["name"]; ?>`s Dashboard</font> </label>
   <?php  include("header1.php"); ?>
   <?php echo '<img class="img" src="user_images/'.$image.'">'; ?>
 </div>
 </nav>
<div id="wrapper" class="toggled">
 <div class="container-fluid">
 <!-- Sidebar -->
 <div id="sidebar-wrapper">
<div class="profile">
<?php 
echo '<img src="user_images/'.$image.'" alt="">';
echo '<p class="name">'.$_SESSION["name"].'</p>';
?>
</div>
<div class="links">
<ul>
 <li>
 <a href="user.php"><i class="fa fa-tasks"></i> <font style="color:white"> All Posts </font></a>
 </li>
 <li>
 <a href="my_task.php"><i class="fas fa-palette"></i><font style="color:white"> My posts </font></a>
 </li>
 <li>
 <a href="ourskill.php"> <i class="fa fa-id-card"></i><font style="color:white"> My Profile </font></a>
 </li>
 <li>
 <a href="notification.php"><i class="fa fa-bell"></i> <font style="color:white">nitification(<?php echo $count; ?>)</font></a>
 </li>
 <li>
 <a href="changepass.php"><i class="fa fa-unlock-alt"></i><font style="color:white">Change Password</font></a>
 </li>
</ul>
</div>
 </div>
 </div>
 </div>
 </div>
 	     <div class="col-lg-8">

            <!--left-content-->
			<div class="center">
				<div class="posts">
					<div class="create-posts">
						<?php 
						   $sear=$_POST["search"];
                            $sql=mysqli_query($con,"select * from posts  where status_title like '%$sear%'");
                            if(mysqli_num_rows($sql))
                            {
                                while($row=mysqli_fetch_array($sql))
                               {
     	                           $id=$row["post_id"];
     	                           $up=$row["usr_id_p"];
     	                           $time=$row["status_time"];
     	                           $sql5=mysqli_query($con,"select * from registration  where usr_id='$up'");
                             while($row5=mysqli_fetch_array($sql5))
                               {
								  $imp=$row5['image'];
								  $name=$row5["name"];
                               }

						echo '<div class="post-show">
									<div class="post-show-inner">
										<div class="post-header">
											<div class="post-left-box">
												<div><img src="user_images/'.$imp.'" class="imgs"></img></div>
												<div class="id-name">
													<ul>
													<p class="name">'.$name.' </p>
														<li class="time">'.timeAgo($time).'</li>
													</ul>
												</div>
											</div>
											<div class="post-right-box"></div>
										</div>
									
											<div class="post-body">
											<div class="post-header-text">
							 <a href="project_description.php?id='.$id.'&s_title=' . $row['status'] . '" class="status">
							  '.$row['status_title'].'</a>
							  
							                 <p class="status-1">'.$row['status'].'</p>

											 <br/><br/>';
										$sql1=mysqli_query($con,"select * from comments where post_id_c='$id'");
										while($row1=mysqli_fetch_array($sql1))
										{	 
                                            $ct=$row1["comment_time"];
                                            $c=$row1["comment"];
                                            $uid=$row1["user_id_c"];
                                            $sql2=mysqli_query($con,"select * from registration where usr_id='$uid'");
                                            while($row2=mysqli_fetch_array($sql2))
										     {
                                                 $n=$row2["name"];
												 $img=$row2["image"];
										     }
										echo '
										<i class="fa fa-external-link-alt"></i>
										
										';
											}
							echo '
									</div>
								</div><br> ';	
     	                           
							   }
							   
							   $messa="";
                            }
                            else
                            {
                            	$messa='<h1 style="color:red">No Post Found...</h1>';
                            }
                              ?>

 	     </div>
 	           <?php echo $messa ?>
     <div>

</div>
</div>
</div>
</div>
</div>
<script src="components/bootstrap/dist/js/bootstrap.min.js"></script>
<script>
 $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });
</script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('.status').click(function() { $('.arrow').css("left", 0);});
			$('.photos').click(function() { $('.arrow').css("left", 146);});
		});
	</script>
	

</body>
</html>