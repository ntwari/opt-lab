<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>OPT lab</title>
    <link rel="stylesheet" href="styles/style.css">
</head>
<body>
    <nav class="nav" id="page-top">
        <div class="logo">
        <a href="index.php">
        <p><span>OPT</span>Lab</p>
        </a>
        </div>
        <div class="search">
            <div class="search_box">
                <input type="text" placeholder="Find Any Thing">
                <i class="fas fa-search"></i>
            </div>
        </div>
        <div class="nav-buttons">
            <a href="User/signin.php">Log In</a>
            <a href="User/signup.php">Sign Up</a>
        </div>
    </nav>
    <div class="main-body">
        <div class="simple-desc">
            <h3>Apply for Your Jop</h3>
            <P>Anywhere,anytime,anyknowledge Apply for it, No limit the limit is only by yourself,
                Enjoy the opportunity with us.
            </P>
            <div class="explore-more">
                <button onclick="openAbout()">Explore More</button>
            </div>
            <!-- <div class="desc_search_box">
                <input type="text" placeholder="Find Any Kind Of Company">
                <i class="fas fa-search"></i>
            </div> -->
        </div>
<div class="space">
    <div class="container">
        <div class="row">
            <div class="m-auto col-xs-6 col-md-8 col-sm-12">
            <div class="companies">
                <i class="fas fa-building"></i>
                <div>
                    <h4>100000<span>Companies</span></h4>
                </div>
            </div>
            <div class="users">
                <i class="fas fa-users"></i>
                <div>
                    <h4>350000<span>Applicants</span></h4>
                </div>
            </div>
            <div class="job-oportunities">
                <i class="fas fa-users"></i>
                <div>
                    <h4>100000<span>Job Opportuniteis</span></h4>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
        <div class="available">
            <h3>Latest Posted Jobs</h3>
            <div class="latest">
            <div class="available-job">
                <img src="images/irembo.png" class="rounded" alt="irembo">
                <div class="location-position">
                    <a href="#">Full Stack Developer</a><br>
                    <!-- <span><i class="fa fa-building"></i>-->Irembo,
                    <!--<i class="fa fa-map-marker"></i>-->&emsp;Kigali,Rwanda. <br>
                    <!--<i class="fa fa-calendar" aria-hidden="true"></i>-->&emsp;2 Months remaining</span>
                </div>
                <div class="apply-btn">
                    <a href="user/signin.php">Apply Now</a>
                </div>
            </div>
            <div class="available-job">
                <img src="images/bk.jpeg" class="rounded" alt="bk">
                <div class="location-position">
                    <a href="#">Full Stack Developer</a><br>
                    <!-- <span><i class="fa fa-building"></i>-->Bank Of Kigali,
                    <!--<i class="fa fa-map-marker"></i>-->&emsp;Kigali,Rwanda. <br>
                    <!--<i class="fa fa-calendar" aria-hidden="true"></i>-->&emsp;3 Weeks remaining</span>
                </div>
                <div class="apply-btn">
                    <a href="user/signin.php">Apply Now</a>
                </div>
            </div>
            <div class="available-job">
                <img src="images/igihe.png" class="rounded" alt="Igihe">
                <div class="location-position">
                    <a href="#">Front End Developer</a><br>
                    <!-- <span><i class="fa fa-building"></i>-->Igihe Media,
                    <!--<i class="fas fa-map-marker-alt"></i>-->&emsp;Kigali,Rwanda. <br>
                    <!--<i class="fa fa-calendar" aria-hidden="true"></i>-->&emsp;3 Weeks remaining</span>
                </div>
                <div class="apply-btn">
                    <a href="user/signin.php">Apply Now</a>
                </div>
            </div>
            </div>
            <div class="view-all">
                <a href="user/signin.php">View All Posts</a>
            </div>
        </div>
        <div class="jobs-by-category">
            <h2>Jobs by Categoly</h2>
            <div class="works-by-given">
                <ul>
                    <li>
                        <i class="fa fa-caret-right"></i>
                        <a href="#">Graphic Designer <span>(214)</span></a>
                    </li>
                    <li> 
                        <i class="fa fa-caret-right"></i>
                        <a href="#">Engineering Jobs <span>(514)</span></a>

                    </li>
                    <li> 
                        <i class="fa fa-caret-right"></i>
                        <a href="#"> Mainframe Jobs <span>(554)</span></a>
                    </li>
                    <li> 
                        <i class="fa fa-caret-right"></i>
                        <a href="#">  Legal Jobs <span>(1255)</span></a>
                    </li>
                    <li> 
                        <i class="fa fa-caret-right"></i>
                        <a href="#">  IT Jobs <span>(145)</span></a>
                    </li>
                    <li> 
                        <i class="fa fa-caret-right"></i>
                        <a href="#">   PSU Jobs <span>(225)</span></a>
                    </li>
                    <li> 
                        <i class="fa fa-plus-circle"></i>
                        <a href="user/signin.php">View All Categories</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="jobs-by-location">
            <h2>Jobs by Location</h2>
            <div class="works-by-given">
                <ul>
                    <li>
                        <i class="fa fa-caret-right"></i>
                        <a href="#">Kigali <span>(215)</span></a>
                    </li>
                    <li> 
                        <i class="fa fa-caret-right"></i>
                        <a href="#"> Musanze <span>(100)</span></a>

                    </li>
                    <li> 
                        <i class="fa fa-caret-right"></i>
                        <a href="#"> Rwamagana <span>(125)</span></a>
                    </li>
                    <li> 
                        <i class="fa fa-caret-right"></i>
                        <a href="#">  Kenya <span>(200)</span></a>
                    </li>
                    <li> 
                        <i class="fa fa-caret-right"></i>
                        <a href="#">  South Africa <span>(230)</span></a>
                    </li>
                    <li> 
                        <i class="fa fa-plus-circle"></i>
                        <a href="user/signin.php">View All Locations</a>
                    </li>
                </ul>
            </div>
        </div>


<section class="users-here">
  <h1>Users Review</h1>
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="user-review">
          <p>If you are visiting Southern California Los Angeles, we welcome you to a memorable helicopter
            tour flight that will take you around the Los Angeles area from a bird's eye view! </p>
            <h5>Pravel Sharma</h5>
            <small>India</small>
        </div>
        <img src="images/user-1.jpg" alt="">
      </div>
      <div class="col-md-4">
        <div class="user-review">
          <p>If you are visiting Southern California Los Angeles, we welcome you to a memorable helicopter
            tour flight that will take you around the Los Angeles area from a bird's eye view! </p>
            <h5>Pravel Sharma</h5>
            <small>Los Angels</small>
        </div>
        <img src="images/user-2.jpg" alt="">
      </div>
      <div class="col-md-4">
        <div class="user-review">
          <p>If you are visiting Southern California Los Angeles, we welcome you to a memorable helicopter
            tour flight that will take you around the Los Angeles area from a bird's eye view! </p>
            <h5>Pravel Sharma</h5>
            <small>South Africa</small>
        </div>
        <img src="images/user-3.jpg" alt="">
      </div>
    </div>
  </div>
<div class="get-your-space">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="book-your-space">
                    <h3>Book Workspace</h3>
                    <p>Share Opportunity to others,OPTLab gives you a space to
                         have online job seekers.
                    </p>
                    <button onclick="openConfrimation()">Get Workspase</button>
                </div>
            </div><div class="col-md-6">
                <div class="get-workspace">
                    <h3>Apply For Workspase</h3>
                    <p>Get opportunity, OPTLab will connect you to available workspace 
                        to day is the day.
                    </p>
                    <button onclick="openUser()">Get Opportunity</button>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
<div class="confirm-email">
    <div class="confirm-contet">
        <div class="confirm-header">
            <span class="close">&times;</span>
            <h2>Produce your Email first</h2>
        </div>
    </div>
</div>
        <div class="back-top">
            <a class="back-to-top" href="#page-top"><i class="fas fa-chevron-up"></i></a>
        </div>
    </div>

    <script>
        $(document).ready(function(){
            var offset = 250;
            var duration = 500;

            $(window).scroll(function(){
                if($(this).scrollTop() > offset){
                    $('.back-to-top').fadeIn(duration)
                }else{
                    $('.back-to-top').fadeOut(duration)
                }
            })
        })
    </script>

</body>
<script src="javascript/index.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<link rel="stylesheet" href="Bootstrap/css/bootstrap.min.css">
<script type="text/javascript" src="Bootstrap/js/bootstrap.min.js"></script>
<script src="https://kit.fontawesome.com/e5644a9822.js" crossorigin="anonymous"></script>

<!-- <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> -->
<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->
</html>